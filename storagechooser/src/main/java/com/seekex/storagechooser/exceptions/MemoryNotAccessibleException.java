package com.seekex.storagechooser.exceptions;


public class MemoryNotAccessibleException extends Exception {

    public MemoryNotAccessibleException(String message) {
        super(message);
    }
}
