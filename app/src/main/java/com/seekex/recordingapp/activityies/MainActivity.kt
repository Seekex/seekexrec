package com.seekex.recordingapp.activityies

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.preference.PreferenceManager
import android.provider.CallLog
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.readstorage.DatabaseHandler
import com.seekex.readstorage.jobService.RecordingsDBJobService
import com.seekex.recordingapp.Contact
import com.seekex.recordingapp.CustomAdapter
import com.seekex.recordingapp.Detail
import com.seekex.recordingapp.R
import com.seekex.recordingapp.jobService.RecordingsAlarmReceiver
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.*
import com.seekex.scheduleproject.AppConstants
import com.seekex.storagechooser.StorageChooser
import com.seekx.interfaces.PermissionCallBack
import com.seekx.webService.ApiImp
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.user_view_layout.*
import java.io.File
import java.lang.Long
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileTime
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    lateinit var apiImp: ApiImp

    private var coordinatorLayout: CoordinatorLayout? = null
    private var btn: Button? = null
    private val builder = StorageChooser.Builder()
    private var chooser: StorageChooser? = null
    private val TAG = javaClass.name
    internal var permissionCallBack: PermissionCallBack? = null
    private val FOLDERPICKER_CODE = 0
    val MyPREFERENCES = "MyPrefs"
    var counter = 1
    var sharedpreferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null
    lateinit var db: DatabaseHandler
    lateinit var reclist: RecyclerView;
    lateinit var txt: TextView;
    lateinit var pb: ProgressBar;
    var batchSize = 10
    private var call_Counter = 0
    private var missed_call_Counter = 0
    var start = 0
    var end = batchSize
    var filelocation: String = ""
    private lateinit var userAdapter: CustomAdapter
    private var list: ArrayList<Contact> = java.util.ArrayList()
    private var missedList: ArrayList<Contact> = java.util.ArrayList()
    private var datalist: ArrayList<Contact> = java.util.ArrayList()
    private var dateList: ArrayList<Contact> = java.util.ArrayList()


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        coordinatorLayout = findViewById<View>(R.id.coord) as CoordinatorLayout
        btn = findViewById<Button>(R.id.button) as Button
        reclist = findViewById<RecyclerView>(R.id.reclist)
        txt = findViewById<TextView>(R.id.txt) as TextView
        pb = findViewById<ProgressBar>(R.id.pb) as ProgressBar
        apiImp = ApiImp(this)

        db = DatabaseHandler(this)
        setadapter()
        setPrefernce()
        setHeader()
        filelocation = sharedpreferences!!.getString("folderLocation", "")!!
        batteryOptimization()


        checkStoragePermission(object : PermissionCallBack {
            override fun onPermissionAccessed() {

                saveMissedCalldetails()
            }
        })
        try {
            saveMissedCalldetails()
        } catch (e: Exception) {
        }
        getRecordingsFromPath()
//        checkPendingRecforCandId()
        firstInstallSettings()
        rel_setting.setOnClickListener {
            intent = Intent(applicationContext, SettingActivity::class.java)
            startActivity(intent)
        }
        refresh.setOnClickListener {

            apiImp.showProgressBar()
            getRecordingsFromPath()
//            checkPendingRecforCandId()
            showDataFromDataBase()
            getListingData()
//            apiImp.cancelProgressBar()
        }
//        btn!!.setOnClickListener {
//            checkStoragePermission(object : PermissionCallBack {
//                override fun onPermissionAccessed() {
//                    builder.build().show()
//                }
//            })
//        }

    }

    private fun firstInstallSettings() {
        var firstAlarm = sharedpreferences!!.getBoolean("firstalarm", false)
        if (!firstAlarm) {
            setAlarm()
            editor?.putBoolean("firstalarm", true)?.commit()
        }
        var firstDBAlarm = sharedpreferences!!.getBoolean("firstdbalarm", false)
        if (!firstDBAlarm) {
//            setAlarmForDeletion()
            editor?.putBoolean("firstdbalarm", true)?.commit()
        }

        var firstdata = sharedpreferences!!.getBoolean("listingdata", false)
        if (!firstdata) {
            getListingData()
            editor?.putBoolean("listingdata", true)?.commit()
        }
        initializeSwipeRefresh()
//filelocation.monikaHere()
        var firstinstall = sharedpreferences!!.getBoolean("firstInstall", false)
        if (!firstinstall) {
            editor?.putLong("installtime", System.currentTimeMillis())?.commit()
            Log.e(TAG, "time installtime: " + System.currentTimeMillis())
            editor?.putBoolean("firstInstall", true)?.commit()
        }


    }
//
//    private fun checkPendingRecforCandId() {
//        Log.e(TAG, "checkPendingRecforCandId ")
//        var alist = db.getContactnew("1")
//        Log.e(TAG, "checkPendingRecforCandId list size: " + alist.size)
//
//        for (i in alist.indices) {
//            checkifExists(alist.get(i).phoneNumber, alist.get(i).id)
//        }
//    }

    private fun initializeSwipeRefresh() {
        swipe_container.setOnRefreshListener {
            swipeRefresh()

//            Thread {
//                checkPendingRecforCandId()
//                runOnUiThread{
            showDataFromDataBase()
//                }
//            }.start()
        }

        // Scheme colors for animation
        swipe_container.setColorSchemeColors(
            resources.getColor(android.R.color.holo_blue_bright),
            resources.getColor(android.R.color.holo_green_light),
            resources.getColor(android.R.color.holo_orange_light),
            resources.getColor(android.R.color.holo_red_light)
        )

    }

    private fun setHeader() {
        header.setText("Call Logs")
        rel_setting.visibility = View.VISIBLE
        refresh.visibility = View.VISIBLE
    }

    private fun batteryOptimization() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent()
            val packageName = packageName
            val pm = getSystemService(POWER_SERVICE) as PowerManager
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                intent.data = Uri.parse("package:$packageName")
                startActivity(intent)
            }
        }
    }

    private fun setadapter() {
        userAdapter = CustomAdapter(datalist, this, { h, duration, rec_url ->
            intent = Intent(applicationContext, Detail::class.java)
            intent.putExtra("rec_number", h)
            intent.putExtra("duration", duration)
            intent.putExtra("rec_url", rec_url)
            startActivity(intent)
        })
        reclist.layoutManager = LinearLayoutManager(this)
        reclist.adapter = userAdapter
    }

    private fun showDataFromDataBase() {
        Log.e(TAG, "showDataFromDataBase: ")

        if (db != null && db.contactsCount > 0 && filelocation!!.length > 0) {
            showDatainList()
        } else {
            datalist.clear()
            setadapter()
        }
    }


    private fun getRecordingsFromPath() {
        filelocation = sharedpreferences!!.getString("folderLocation", "")!!

        if (filelocation!!.length > 0) {
            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    txt.setText("Recordings will be picked from: " + filelocation)

//                    Thread {
                    getRecFiles(filelocation)
//                        runOnUiThread {
                    Log.e(TAG, "runOnUiThread: ")
                    uploadData()

                    swipe_container.isRefreshing = false
                    showDataFromDataBase()
////
//                    }.start()
                }
            })

        } else {
            Toast.makeText(this, "Pick folder location from Settings !!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun swipeRefresh() {
        filelocation = sharedpreferences!!.getString("folderLocation", "")!!

        if (filelocation!!.length > 0) {
            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    txt.setText("Recordings will be picked from: " + filelocation)

                    getRecFiles(filelocation)
                }
            })

        } else {
            Toast.makeText(this, "Pick folder location from Settings !!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setPrefernce() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
    }

    var interestlist: ArrayList<ListingDTO> = java.util.ArrayList()
    var ratinglist: ArrayList<ListingDTO> = java.util.ArrayList()
    var contypelist: ArrayList<ListingDTO> = java.util.ArrayList()
    var statuslist: ArrayList<ListingDTO> = java.util.ArrayList()
    var remarkstatuslist: ArrayList<ListingDTO> = java.util.ArrayList()


    fun getListingData() {

        apiImp.getListing(object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    val ss = any as GetListingRes
                    var list: ArrayList<ListingDTO> = db.allListings
                    if (list.size > 0) {
                        db.deleteListingTable()
                        interestlist.clear()
                        ratinglist.clear()
                        contypelist.clear()
                        statuslist.clear()
                        remarkstatuslist.clear()
                    }

                    interestlist.addAll(ss.interest)
                    remarkstatuslist.addAll(ss.remark_status)
                    statuslist.addAll(ss.job_current_status)
                    contypelist.addAll(ss.conversation)
                    ratinglist.addAll(ss.rating)

                    for (i in interestlist.indices) {
                        db.addListingTable(
                            interestlist.get(i).id,
                            interestlist.get(i).`val`,
                            AppConstants.SPININTEREST
                        )

                    }
                    for (i in remarkstatuslist.indices) {
                        db.addListingTable(
                            remarkstatuslist.get(i).id,
                            remarkstatuslist.get(i).`val`,
                            AppConstants.SPINREMARK_STATUS
                        )

                    }
                    for (i in statuslist.indices) {
                        db.addListingTable(
                            statuslist.get(i).id,
                            statuslist.get(i).`val`,
                            AppConstants.SPINSTATUS_LIST
                        )

                    }
                    for (i in contypelist.indices) {
                        db.addListingTable(
                            contypelist.get(i).id,
                            contypelist.get(i).`val`,
                            AppConstants.SPINCON_TYPE
                        )

                    }
                    for (i in ratinglist.indices) {
                        db.addListingTable(
                            ratinglist.get(i).id,
                            ratinglist.get(i).`val`,
                            AppConstants.SPINRATING_LIST
                        )

                    }
                }
            }
        })
    }

    private fun setAlarm() {
        val alarmManager = this.getSystemService(ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, RecordingsAlarmReceiver::class.java)
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)
        //30 * 1000 30 sec
        //120000 2 min
        //3600000 1 hour
        alarmManager.setRepeating(
            AlarmManager.RTC_WAKEUP,
            System.currentTimeMillis(),
            3600000,
            pendingIntent
        );
        Log.e("Alarm: ", "Alarm is Set.")
//        Toast.makeText(this, "Alarm is set.", Toast.LENGTH_SHORT).show()

    }

    private fun setAlarmForDeletion() {

        val jobScheduler: JobScheduler = applicationContext
            .getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler

        val componentName = ComponentName(
            this,
            RecordingsDBJobService::class.java
        )

        val jobInfo: JobInfo = JobInfo.Builder(1, componentName)
            .setPeriodic(24 * 60 * 60 * 1000L)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
            .setPersisted(true).build()
        jobScheduler.schedule(jobInfo)

//        val alarmManager = this.getSystemService(ALARM_SERVICE) as AlarmManager
////        val intent = Intent(this, RecordingsAlarmDbReceiver::class.java)
////        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
////        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND)
////        val pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)
////        //30 * 1000 30 sec
////        //120000 2 min
////        //3600000 1 hour
////        //1728000000 20 days
////        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1728000000, pendingIntent);
//        Log.e("Alarm: ", "Alarm is Set for deletion.")
//        Toast.makeText(this, "Alarm is set.", Toast.LENGTH_SHORT).show()

    }
/*
    private fun setBuilder() {
        val c = Content()
        c.createLabel = "Create"
        c.internalStorageText = "My Storage"
        c.cancelLabel = "Cancel"
        c.selectLabel = "Select"
        c.overviewHeading = "Select path to pick call recording files"
        builder.withActivity(this)
                .withFragmentManager(fragmentManager)
                .setMemoryBarHeight(1.5f) //                .disableMultiSelect()
                .withContent(c)
        builder.allowCustomPath(true)
        builder.setType(StorageChooser.DIRECTORY_CHOOSER)

        chooser = builder.build()

        builder.build().setOnSelectListener(object : StorageChooser.OnSelectListener {
            override fun onSelect(path: String) {
                Toast.makeText(getApplicationContext(), path, Toast.LENGTH_SHORT).show()
                editor?.putString("folderLocation", path)?.commit()
                txt.setText("Recordings will be picked from: " + path)
                counter = 1
                pb.visibility = View.VISIBLE
                Thread {
                    getRecFiles(path)
                    runOnUiThread {
                        pb.visibility = View.GONE
                        showDatainList()
                    }

                }.start()
            }
        })
        builder.build().setOnCancelListener(StorageChooser.OnCancelListener {
            Toast.makeText(applicationContext, "Storage Chooser Cancelled.", Toast.LENGTH_SHORT).show()
        })


    }
*/

    @SuppressLint("NewApi")

    private fun getRecFiles(folderLocation: String) {
        var pp: String = folderLocation
        val file = File(pp)
        val files = file.listFiles()
        Log.d("Files", "Path: $file")
        try {
            Log.e("Files", "Size: " + files.size)
            if (files.size == 0) {
                Toast.makeText(this, "Selected folder is empty", Toast.LENGTH_LONG).show()
                swipe_container.isRefreshing = false
                return
            }
        } catch (e: Exception) {
        }
        Arrays.sort(files, Comparator.comparingLong(File::lastModified).reversed());


        var fileList = addNewFiles(files)

//        if (fileList.size == 0) {
//            swipe_container.isRefreshing = false
//        }


        for (i in fileList.indices) {

            val filePath: Path = fileList[i].toPath()
            var attr: BasicFileAttributes? = null

            attr = Files.readAttributes(filePath, BasicFileAttributes::class.java)
            val creationTime: FileTime = attr.creationTime()
            val df = SimpleDateFormat("HH:mm")
            val df2 = SimpleDateFormat("dd-MM-yyyy")
            val timeCreated = df.format(creationTime.toMillis())
            val dateCreated = df2.format(creationTime.toMillis())

            Log.e(
                "Files",
                "FileName:" + files[i].name + " Filepath:" + fileList[i].absolutePath + " file creationTime:" + creationTime + " dateCreated:" + dateCreated + " creationTime:" + timeCreated
            )


            Log.e("TAG", "getRecFiles: " + counter + " " + files.size)
            if (!db.isPathExists(fileList[i].absolutePath.toString())) {
                getCallDetails(
                    files.size,
                    db,
                    dateCreated,
                    timeCreated,
                    fileList[i].absolutePath.toString(),
                    creationTime.toMillis().toLong()
                )
            }
        }

        uploadData()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun addNewFiles(files: Array<File>?): ArrayList<File> {
        val fileList = ArrayList<File>()
        var tb_count: Int = db.contactsCount
        for (i in files!!.indices) {
            val filePath: Path = files[i].toPath()
            var attr: BasicFileAttributes? = null

            attr = Files.readAttributes(filePath, BasicFileAttributes::class.java)
            val creationTime: FileTime = attr.creationTime()
            var installTime = sharedpreferences!!.getLong("installtime", 0)
            val creationTimeMillis = creationTime.toMillis()
            if (creationTimeMillis > installTime) {
                if (tb_count == 0) {
                    fileList.add(files[i])
                } else {
                    if (!db.isPathExists(files[i].absolutePath.toString())) {
                        fileList.add(files[i])
                    }
                }
                Log.e("time", " fileList: " + fileList + " installtime " + installTime)
            }
        }
        return fileList
    }

    private fun uploadData() {
        swipe_container.isRefreshing = false
        call_Counter = 0
        list.clear()
        list = db.getContact("1")
        Log.e(TAG, "uploadData: " + list.size)
        if (list.size == 0) {
//            Toast.makeText(applicationContext, "No New Record to Upload!", Toast.LENGTH_SHORT).show()
            uploadMissedCallData()
            return
        }
        uploadFileNew()
    }


    private fun uploadMissedCallData() {
        swipe_container.isRefreshing = false
        missed_call_Counter = 0
        missedList.clear()
        missedList = db.getContact("missed")
        Log.e(TAG, "uploadMissedCallData: " + missedList.size)
        if (missedList.size == 0) {
//            Toast.makeText(applicationContext, "No New Record to Upload!", Toast.LENGTH_SHORT).show()
            return
        }
        uploadMissCallData()
    }

    private fun getCallDetails(
        filesize: Int,
        db: DatabaseHandler,
        recDateCreated: String,
        recTimeCreated: String,
        absolutePath: String,
        rec_time_millis: kotlin.Long
    ) {

        var fromtime = sharedpreferences!!.getLong("installtime", 0)
        val formatter2 = SimpleDateFormat("dd-MM-yyyy")
        val dateString: String = formatter2.format(Date(fromtime))
        Log.e("dateString", "getCallDetails: $dateString")


        val managedCursor: Cursor? = applicationContext.contentResolver.query(
            CallLog.Calls.CONTENT_URI, arrayOf(
                CallLog.Calls.TYPE, CallLog.Calls.DATE, CallLog.Calls.DURATION,
                CallLog.Calls.NUMBER, CallLog.Calls._ID
            ),
            CallLog.Calls.DATE + ">?", arrayOf(java.lang.String.valueOf(fromtime)),
            CallLog.Calls.NUMBER + " asc"
        )


        val number = managedCursor?.getColumnIndex(CallLog.Calls.NUMBER)
        val type = managedCursor?.getColumnIndex(CallLog.Calls.TYPE)
        val date = managedCursor?.getColumnIndex(CallLog.Calls.DATE)
        val duration = managedCursor?.getColumnIndex(CallLog.Calls.DURATION)
//        val name = managedCursor?.getColumnIndex(CallLog.Calls.CACHED_NAME)

        while (managedCursor!!.moveToNext()) {


            val phNumber = managedCursor.getString(number!!)
//            val phname = managedCursor.getString(name!!)
            val callType = managedCursor.getString(type!!)
            val callDate = managedCursor.getString(date!!)
            val callDayTime = Date(Long.valueOf(callDate))
            val callDuration = managedCursor.getString(duration!!)
            var dir: String = ""
            val dircode: Int = callType!!.toInt()
            when (dircode) {
                CallLog.Calls.OUTGOING_TYPE -> dir = "OUTGOING"
                CallLog.Calls.INCOMING_TYPE -> dir = "INCOMING"
                CallLog.Calls.MISSED_TYPE -> dir = "MISSED"
            }

            Log.e("calldetails", ": $phNumber")

            var calldurLng = callDuration.toLong()
            val calldurationInMillis =
                calldurLng * 1000 // adding millis to call duration for calculation

            val formatCalldate = callDate.toLong()// call datetime in mills
            var totalCallTime = formatCalldate + calldurationInMillis// call duration + call time
            var diff = rec_time_millis - totalCallTime
            val formatter = SimpleDateFormat("HH:mm")
            val formatter2 = SimpleDateFormat("dd-MM-yyyy")
            val formatter3 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val timeString: String = formatter.format(Date(formatCalldate))
            val dateString: String = formatter2.format(Date(formatCalldate))
            val datetimeforapi: String = formatter3.format(Date(formatCalldate))
            val endtimecal: String = formatter.format(totalCallTime)
            val tomillisecal: String = formatter.format(rec_time_millis)
            var recNumber = getRecordingNumber()

            if (endtimecal.equals(tomillisecal) && callDuration.toLong() > 0 && recDateCreated.equals(
                    dateString
                )
            ) {

                db.addContact(
                    datetimeforapi,
                    "",
                    phNumber,
                    callDuration + " sec",
                    absolutePath,
                    "1",
                    "",
                    recNumber.toString(),
                    "",
                    dircode.toString(),
                    endtimecal,
                    dateString
                )
                showDataFromDataBase()
            } else {
                if ((diff > 0 && diff < 20000) && callDuration.toLong() > 0 && recDateCreated.equals(
                        dateString
                    )
                ) {
                    db.addContact(
                        datetimeforapi,
                        "",
                        phNumber,
                        callDuration + " sec",
                        absolutePath,
                        "1",
                        "",
                        recNumber.toString(),
                        "",
                        dircode.toString(),
                        endtimecal,
                        dateString
                    )
                    showDataFromDataBase()
                }
            }
        }
        managedCursor.close()
        Log.e("counter", "getCallDetails: $counter -- $filesize")
    }

    private fun saveMissedCalldetails() {

        var fromtime = sharedpreferences!!.getLong("installtime", 0)
        val formatter2 = SimpleDateFormat("dd-MM-yyyy")
        val dateString: String = formatter2.format(Date(fromtime))
        Log.e("dateString", "getCallDetails: $dateString")


        val managedCursor: Cursor? = applicationContext.contentResolver.query(
            CallLog.Calls.CONTENT_URI, arrayOf(
                CallLog.Calls.TYPE, CallLog.Calls.DATE, CallLog.Calls.DURATION,
                CallLog.Calls.NUMBER, CallLog.Calls._ID
            ),
            CallLog.Calls.DATE + ">?", arrayOf(java.lang.String.valueOf(fromtime)),
            CallLog.Calls.NUMBER + " asc"
        )


        val number = managedCursor?.getColumnIndex(CallLog.Calls.NUMBER)
        val type = managedCursor?.getColumnIndex(CallLog.Calls.TYPE)
        val date = managedCursor?.getColumnIndex(CallLog.Calls.DATE)
        val duration = managedCursor?.getColumnIndex(CallLog.Calls.DURATION)
//        val name = managedCursor?.getColumnIndex(CallLog.Calls.CACHED_NAME)

        while (managedCursor!!.moveToNext()) {


            val phNumber = managedCursor.getString(number!!)
//            val phname = managedCursor.getString(name!!)
            val callType = managedCursor.getString(type!!)
            val callDate = managedCursor.getString(date!!)
            val callDayTime = Date(Long.valueOf(callDate))
            val callDuration = managedCursor.getString(duration!!)
            var dir: String = ""
            val dircode: Int = callType!!.toInt()
            when (dircode) {
                CallLog.Calls.OUTGOING_TYPE -> dir = "OUTGOING"
                CallLog.Calls.INCOMING_TYPE -> dir = "INCOMING"
                CallLog.Calls.MISSED_TYPE -> dir = "MISSED"
            }

            Log.e("calldetails", ": $phNumber")
            Log.e("duration", ": $callDuration")

            var calldurLng = callDuration.toLong()
            val calldurationInMillis =
                calldurLng * 1000 // adding millis to call duration for calculation

            val formatCalldate = callDate.toLong()// call datetime in mills
            var totalCallTime = formatCalldate + calldurationInMillis// call duration + call time
            val formatter = SimpleDateFormat("HH:mm")
            val formatter2 = SimpleDateFormat("dd-MM-yyyy")
            val formatter3 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val timeString: String = formatter.format(Date(formatCalldate))
            val dateString: String = formatter2.format(Date(formatCalldate))
            val datetimeforapi: String = formatter3.format(Date(formatCalldate))

            val endtimecal: String = formatter.format(totalCallTime)

            if (dir.equals("MISSED") || callDuration.equals("0")) {
                Log.e(TAG, "saveMissedCalldetails: " + datetimeforapi)
                if (!db.isDatetimeExists(datetimeforapi)) {
                    db.addContact(
                        datetimeforapi,
                        "",
                        phNumber,
                        "0 sec",
                        "",
                        "missed",
                        "",
                        "",
                        "",
                        dircode.toString(),
                        endtimecal,
                        dateString
                    )
                }
            }

        }
        managedCursor.close()
    }

    fun checkifExists(phNumber: String, id: String) {
        Log.e(TAG, "checkifExists: $phNumber id $id")
        apiImp.checkCanNumber(phNumber, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e(TAG, "onSuccess: filepath=")
                val ss = any as CheckNumberREs
                if (ss.success == 1) {
                    db.updateContact(ss.candidate_id, id)
                    apiImp.hitApi(call_Counter, list, object : ApiCallBack {
                        override fun onSuccess(status: Boolean, any: Any) {
                            Log.e("TAG", "onSuccess: filepath=")

                            if (status) {
                                var ss = any as GetRecordingsRes
                                var filepath = ss.data.path + ss.data.filename
                                Log.e("TAG", "onSuccess: filepath=$filepath")
                                apiImp.hitDataApi(
                                    call_Counter,
                                    list,
                                    filepath,
                                    object : ApiCallBack {
                                        override fun onSuccess(status: Boolean, any: Any) {
                                            if (status) {

                                                db.updateContact(
                                                    "0",
                                                    filepath,
                                                    list.get(call_Counter).id
                                                )
                                                showDataFromDataBase()
                                                if (call_Counter < list.size) {
                                                    call_Counter++
                                                    uploadFileNew()
                                                }
                                            }
                                        }
                                    })
                            }
                        }
                    })
                } else {
                    apiImp.hitApi(call_Counter, list, object : ApiCallBack {
                        override fun onSuccess(status: Boolean, any: Any) {
                            Log.e("TAG", "onSuccess: filepath=")

                            if (status) {
                                var ss = any as GetRecordingsRes
                                var filepath = ss.data.path + ss.data.filename
                                Log.e("TAG", "onSuccess: filepath=$filepath")
                                apiImp.hitNonCandidateDataApi(
                                    call_Counter,
                                    list,
                                    filepath,
                                    object : ApiCallBack {
                                        override fun onSuccess(status: Boolean, any: Any) {
                                            if (status) {
                                                Log.e(
                                                    "TAG",
                                                    "onSuccess 11: filepath=${list.get(call_Counter).rec_file_path}"
                                                )

                                                db.updateContact(
                                                    "0",
                                                    filepath,
                                                    list.get(call_Counter).id
                                                )
                                                showDataFromDataBase()
                                                if (call_Counter < list.size) {
                                                    call_Counter++
                                                    uploadFileNew()
                                                }
                                            }
                                        }
                                    })
                            }
                        }
                    })
                }
            }
        })
    }

    fun checkCanIdExists(
        phNumber: String,
        datetimeforapi: String,
        phname: String,
        callDuration: String,
        absolutePath: String,
        statuss: String,
        recnumber: String,
        recurl: String,
        dir: String,
        endtimecal: String,
        dateString: String
    ) {
        apiImp.checkCanNumber(phNumber, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e(TAG, "onSuccess: filepath=")
                val ss = any as CheckNumberREs
                if (ss.success == 1) {
                    db.addContact(
                        datetimeforapi,
                        phname,
                        phNumber,
                        callDuration,
                        absolutePath,
                        statuss,
                        ss.candidate_id,
                        recnumber,
                        recurl,
                        dir,
                        endtimecal,
                        dateString
                    )
                    showDataFromDataBase()
                    return
                } else {
                    db.addContact(
                        datetimeforapi,
                        phname,
                        phNumber,
                        callDuration,
                        absolutePath,
                        statuss,
                        "",
                        recnumber,
                        recurl,
                        dir,
                        endtimecal,
                        dateString
                    )
                    showDataFromDataBase()
                    return
                }
            }
        })
    }

    private fun getRecordingNumber(): Int {
        val rand = Random()
        val abcd = rand.nextInt(100)
        return abcd
    }


    private fun showDatainList() {
        datalist.clear()
        datalist = db.allContacts
        userAdapter = CustomAdapter(datalist, this, { h, duration, rec_url ->
            intent = Intent(applicationContext, Detail::class.java)
            intent.putExtra("rec_number", h)
            intent.putExtra("duration", duration)
            intent.putExtra("rec_url", rec_url)
            startActivity(intent)
        })
        Log.e(TAG, "showDatainList: ${list.size}")
        reclist.layoutManager = LinearLayoutManager(this)
        reclist.adapter = userAdapter
        userAdapter.notifyDataSetChanged()
    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean("request_permissions", false)
                .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    private fun uploadFileNew() {
        Log.e("TAG", "uploadFileNew: call_Counter = " + call_Counter)
        checkifExists(list.get(call_Counter).phoneNumber, list.get(call_Counter).id)
    }

    private fun uploadMissCallData() {
        Log.e("TAG", " missed_call_Counter = " + missed_call_Counter)
        Log.e("TAG", " list size = " + missedList.size)

        apiImp.saveMissedCallData(missed_call_Counter, missedList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e("TAG", "onSuccess: filepath=")

                if (status) {
                    db.updateContact("0", "", missedList.get(missed_call_Counter).id)

                    if (missed_call_Counter < missedList.size - 1) {
                        missed_call_Counter++
                        uploadMissCallData()
                    }
                }
            }

        })
    }
}