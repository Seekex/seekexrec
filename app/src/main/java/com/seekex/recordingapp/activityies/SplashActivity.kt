package com.seekex.recordingapp.activityies

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.seekex.recordingapp.R
import com.seekx.webService.ApiImp

class SplashActivity : AppCompatActivity() {

    var editor: SharedPreferences.Editor? = null
    lateinit var apiImp: ApiImp
    var sharedpreferences: SharedPreferences? = null
    val MyPREFERENCES = "MyPrefs"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        apiImp = ApiImp(this)
        setPrefernce()
        Handler().postDelayed({ // This method will be executed once the timer is over

            if (sharedpreferences?.getBoolean("islogin",false)!!){
                val i = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(i)
                finish()
            }else{
                val i = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(i)
                finish()
            }

        }, 3000)
    }

    private fun setPrefernce() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
    }

}