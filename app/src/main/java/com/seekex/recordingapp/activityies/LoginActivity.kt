package com.seekex.recordingapp.activityies

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.seekex.recordingapp.R
import com.seekex.recordingapp.retrofitclasses.OtpVerification
import com.seekex.recordingapp.retrofitclasses.models.apiRequest.LoginReq
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.LoginRes
import com.seekx.webService.ApiImp
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.login.*

class LoginActivity : AppCompatActivity() {

    var editor: SharedPreferences.Editor? = null
    lateinit var apiImp: ApiImp
    var sharedpreferences: SharedPreferences? = null
    val MyPREFERENCES = "MyPrefs"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        apiImp = ApiImp(this)
        setPrefernce()

        btn_login.setOnClickListener {

            if (et_mobile.text.toString().trim().length>0){
                login()
            }else{
                apiImp.dialogUtil.showAlert("Enter Number")
            }

        }
    }

    fun login() {
        apiImp.showProgressBar()
        val loginreq = LoginReq("send_otp_mobile_number", et_mobile.text.toString().trim())
        apiImp.login(loginreq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                apiImp.cancelProgressBar()
                Log.e("TAG", "onSuccess: login=")
                val ss = any as LoginRes
                if (ss.userFound.equals("1")) {
//                    editor?.putBoolean("islogin", true)?.commit()

                    intent = Intent(applicationContext, OtpVerification::class.java)
                    intent.putExtra("number",et_mobile.text.toString().trim())
                    startActivity(intent)
                } else {
                    apiImp.dialogUtil.showAlert(ss.msg)
                }

            }
        })


    }

    private fun setPrefernce() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
    }

}