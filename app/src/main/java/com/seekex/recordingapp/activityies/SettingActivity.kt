package com.seekex.recordingapp.activityies

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.seekex.readstorage.DatabaseHandler
import com.seekex.recordingapp.R
import com.seekex.recordingapp.jobService.RecordingsAlarmReceiver
import com.seekex.storagechooser.Content
import com.seekex.storagechooser.StorageChooser
import com.seekx.interfaces.PermissionCallBack
import com.seekx.webService.ApiImp
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.settingactivity.*

class SettingActivity : AppCompatActivity() {
    private val builder = StorageChooser.Builder()
    private var chooser: StorageChooser? = null
    var sharedpreferences: SharedPreferences? = null
    val MyPREFERENCES = "MyPrefs"
    internal var permissionCallBack: PermissionCallBack? = null
    lateinit var apiImp: ApiImp
    lateinit var db: DatabaseHandler

    var editor: SharedPreferences.Editor? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settingactivity)
        apiImp = ApiImp(this)
        db = DatabaseHandler(this)
        setHeader()
        setPrefernce()
        setBuilder()
        var filelocation = sharedpreferences!!.getString("folderLocation", "")
        if (filelocation!!.length > 0) {
            txt_pathpicked.setText("Recordings will be picked from: " + filelocation)
        }

        logout.setOnClickListener {

            editor!!.putBoolean("islogin", false).commit()
            editor!!.putString("folderLocation", "").commit()
            editor!!.putBoolean("firstInstall", false).commit()
            db.delete()
            finish()
            RecordingsAlarmReceiver.cancelJob(this)
            val i = Intent(this, LoginActivity::class.java)
            i.flags =  Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)


        }
        choosepath.setOnClickListener {

            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    builder.build().show()

                }

            })

        }
    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.READ_CALL_LOG,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                    .putBoolean("request_permissions", false)
                    .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    private fun setHeader() {
        header.setText("Settings")
        back_icon.visibility = View.VISIBLE
        back_icon.setOnClickListener {
            finish()
        }
    }

    private fun setPrefernce() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
    }

    private fun setBuilder() {
        val c = Content()
        c.createLabel = "Create"
        c.internalStorageText = "My Storage"
        c.cancelLabel = "Cancel"
        c.selectLabel = "Select"
        c.overviewHeading = "Select path to pick call recording files"
        builder.withActivity(this)
                .withFragmentManager(fragmentManager)
                .setMemoryBarHeight(1.5f) //                .disableMultiSelect()
                .withContent(c)
        builder.allowCustomPath(true)
        builder.setType(StorageChooser.DIRECTORY_CHOOSER)

        chooser = builder.build()

        builder.build().setOnSelectListener(object : StorageChooser.OnSelectListener {
            override fun onSelect(path: String) {
                Toast.makeText(getApplicationContext(), path, Toast.LENGTH_SHORT).show()
                editor?.putString("folderLocation", path)?.commit()
                txt_pathpicked.setText("Recordings will be picked from: " + path)

            }
        })
        builder.build().setOnCancelListener(StorageChooser.OnCancelListener {
            Toast.makeText(applicationContext, "Storage Chooser Cancelled.", Toast.LENGTH_SHORT).show()
        })


    }

}
