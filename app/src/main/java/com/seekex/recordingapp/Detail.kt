package com.seekex.recordingapp

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.seekex.readstorage.DatabaseHandler
import com.seekex.recordingapp.adapters.*
import com.seekex.recordingapp.retrofitclasses.models.apiRequest.ListingReq
import com.seekex.recordingapp.retrofitclasses.models.apiRequest.SendDetailReq
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.CheckNumberREs
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.GetDetailRes
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.GetListingRes
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.ListingDTO
import com.seekex.scheduleproject.AppConstants
import com.seekx.webService.ApiImp
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.details.*
import kotlinx.android.synthetic.main.header.*


class Detail : AppCompatActivity() {

    private lateinit var adapterInterest: SpinnerAdapter_Interest
    private lateinit var adapterStatus: SpinnerAdapter_status
    private lateinit var adapterRating: SpinnerAdapter_rating
    private lateinit var adapterComLevel: SpinnerAdapter_Com_level
    private lateinit var adapterRemstatus: SpinnerAdapter_Remstatus
    var interestlist: ArrayList<ListingDTO> = java.util.ArrayList()
    var ratinglist: ArrayList<ListingDTO> = java.util.ArrayList()
    var contypelist: ArrayList<ListingDTO> = java.util.ArrayList()
    var statuslist: ArrayList<ListingDTO> = java.util.ArrayList()
    var remarkstatuslist: ArrayList<ListingDTO> = java.util.ArrayList()
    lateinit var db: DatabaseHandler

    var editor: SharedPreferences.Editor? = null
    lateinit var apiImp: ApiImp
    var sharedpreferences: SharedPreferences? = null
    val MyPREFERENCES = "MyPrefs"

    var interestId: String = "0"
    var contypeId: String = "0"
    var statusId: String = "0"
    var remarkStatusId: String = "0"
    var ratingId: String = "0"
    var rec_number: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details)
        apiImp = ApiImp(this)
        db = DatabaseHandler(this)

        rec_number = intent.getStringExtra("rec_number").toString()
        Log.e("TAG", "onCreate:rec_number " + rec_number)
        setheader()
        setPrefernce()

        txt_calllength.setText(intent.getStringExtra("duration").toString())
        txt_reading.setText(intent.getStringExtra("rec_url").toString())
//        txtx_name.setText(sharedpreferences?.getString("username", ""))



        adapterSettings()
        getListingData()

        getDetails(rec_number, intent.getStringExtra("rec_url").toString())


        btn_submit.setOnClickListener {

            if (contypeId.equals("0") || interestId.equals("0") || ratingId.equals("0") || remarkStatusId.equals("0") || statusId.equals("0") || edt_remark.text.equals("")) {
                Toast.makeText(this, "Please enter Details", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val sendDetailReq = SendDetailReq("update_call_recordings", contypeId, interestId, edt_remark.text.toString().trim(), ratingId, rec_number, "2", statusId, remarkStatusId)

            apiImp.hitDetailApi(sendDetailReq, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    Log.e("TAG", "onSuccess: sendDetailReq=")
                    if (status) {
                        Toast.makeText(this@Detail, "Record saved!!", Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            })

        }
        spin_interest.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View,
                                        position: Int, id: Long) {
                val user: ListingDTO = adapterInterest.getItem(position)!!
                if (user.id != "0") {

                    interestId = user.id
                }
            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })
        spin_rating.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View,
                                        position: Int, id: Long) {
                val user: ListingDTO = adapterRating.getItem(position)!!
                if (user.id != "0") {
                    ratingId = user.id

                }
            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })
        spin_conlevel.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View,
                                        position: Int, id: Long) {
                val user: ListingDTO = adapterComLevel.getItem(position)!!
                if (user.id != "0") {
                    contypeId = user.id
                }
            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })
        spin_remarkstatus.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View,
                                        position: Int, id: Long) {
                val user: ListingDTO = adapterRemstatus.getItem(position)!!
                if (user.id != "0") {
                    remarkStatusId = user.id

                }
            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })

        spin_status.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View,
                                        position: Int, id: Long) {
                val user: ListingDTO = adapterStatus.getItem(position)!!
                if (user.id != "0") {
                    statusId = user.id
                }
            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })
    }

    private fun adapterSettings() {
        adapterInterest = SpinnerAdapter_Interest(applicationContext, interestlist)
        spin_interest.setAdapter(adapterInterest)

        adapterRating = SpinnerAdapter_rating(applicationContext, ratinglist)
        spin_rating.setAdapter(adapterRating)

        adapterStatus = SpinnerAdapter_status(applicationContext, statuslist)
        spin_status.setAdapter(adapterStatus)

        adapterRemstatus = SpinnerAdapter_Remstatus(applicationContext, remarkstatuslist)
        spin_remarkstatus.setAdapter(adapterRemstatus)

        adapterComLevel = SpinnerAdapter_Com_level(applicationContext, contypelist)
        spin_conlevel.setAdapter(adapterComLevel)
    }

    fun getDetails(rec_number: String, rec_url: String) {
        apiImp.getDetails(rec_number, rec_url, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                val ss = any as GetDetailRes
                txtx_name.setText(ss.candidate_name)
                if (ss.conversation_id != "0") {
                    spin_conlevel.isEnabled = false
                    spin_rating.isEnabled = false
                    spin_status.isEnabled = false
                    spin_remarkstatus.isEnabled = false
                    spin_interest.isEnabled = false
                    edt_remark.isEnabled = false
                    btn_submit.visibility = View.INVISIBLE

                    var ss1 = ListingDTO(ss.interest_id)
                    var ss2 = ListingDTO(ss.conversation_id)
                    var ss3 = ListingDTO(ss.candidate_rating)
                    var ss4 = ListingDTO(ss.remark_status_id)
                    var ss5 = ListingDTO(ss.candidate_status)

                    if (interestlist.contains(ss1)) {
                        var index = interestlist.indexOf(ss1)
                        spin_interest.setSelection(index)
                    }
                    if (contypelist.contains(ss2)) {
                        var index = contypelist.indexOf(ss2)
                        spin_conlevel.setSelection(index)
                    }
                    if (ratinglist.contains(ss3)) {
                        var index = ratinglist.indexOf(ss3)
                        spin_rating.setSelection(index)
                    }
                    if (remarkstatuslist.contains(ss4)) {
                        var index = remarkstatuslist.indexOf(ss4)
                        spin_remarkstatus.setSelection(index)
                    }
                    if (statuslist.contains(ss5)) {
                        var index = statuslist.indexOf(ss5)
                        spin_status.setSelection(index)
                    }
                    txtx_name.setText(ss.candidate_name)

                    edt_remark.setText(ss.remarks)
//                    spin_conlevel.setSelection(ss.conversation_id.toInt())
//                    spin_rating   .setSelection(ss.candidate_rating.toInt())
//                    spin_status.setSelection(ss.candidate_status.toInt())
//                    spin_remarkstatus.setSelection(ss.remark_status_id.toInt())
//                    spin_interest.setSelection(ss.interest_id.toInt())
//                    txt_calllength.setText(ss.duration)
                }
            }
        })
    }

    private fun setPrefernce() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
    }

    fun getListingData() {
        var list: ArrayList<ListingDTO> = java.util.ArrayList()

        list = db.allListings
        if (list.size > 0) {

        }
        for (i in list!!.indices) {

            if (list.get(i).type.equals(AppConstants.SPININTEREST)) {
                interestlist.add(list.get(i))
                adapterInterest.notifyDataSetChanged()

            } else if (list.get(i).type.equals(AppConstants.SPINCON_TYPE)) {
                contypelist.add(list.get(i))
                adapterComLevel.notifyDataSetChanged()
            } else if (list.get(i).type.equals(AppConstants.SPINRATING_LIST)) {
                ratinglist.add(list.get(i))
                adapterRating.notifyDataSetChanged()
            } else if (list.get(i).type.equals(AppConstants.SPINREMARK_STATUS)) {
                remarkstatuslist.add(list.get(i))
                adapterRemstatus.notifyDataSetChanged()
            } else if (list.get(i).type.equals(AppConstants.SPINSTATUS_LIST)) {
                statuslist.add(list.get(i))
                adapterStatus.notifyDataSetChanged()
            }

        }


        /* apiImp.getListing(object : ApiCallBack {
             override fun onSuccess(status: Boolean, any: Any) {
                 if (status) {
                     val ss = any as GetListingRes

 //                    adapterRating = SpinnerAdapter_rating(applicationContext, ss.rating)
 //                    spin_rating.setAdapter(adapterRating)
                     interestlist.addAll(ss.interest)
                     adapterInterest.notifyDataSetChanged()

                     remarkstatuslist.addAll(ss.remark_status)
                     adapterRemstatus.notifyDataSetChanged()

                     statuslist.addAll(ss.job_current_status)
                     adapterStatus.notifyDataSetChanged()

                     contypelist.addAll(ss.conversation)
                     adapterComLevel.notifyDataSetChanged()

                     ratinglist.addAll(ss.rating)
                     adapterRating.notifyDataSetChanged()


                 }

             }
         })*/
    }

    private fun setheader() {
        back_icon.visibility = View.VISIBLE
        header.setText("Details")
        back_icon.setOnClickListener {
            finish()
        }
    }
}