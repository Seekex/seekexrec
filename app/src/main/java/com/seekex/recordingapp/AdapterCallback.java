package com.seekex.recordingapp;

public interface AdapterCallback {
    void onSingleClick(Object object);
}
