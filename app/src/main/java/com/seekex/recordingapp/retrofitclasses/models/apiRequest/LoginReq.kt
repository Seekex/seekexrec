package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class LoginReq(var service_name: String?,var mobile_no: String?): BaseRequest()
