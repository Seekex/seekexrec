package com.seekex.recordingapp.retrofitclasses

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.seekex.recordingapp.activityies.MainActivity
import com.seekex.recordingapp.R
import com.seekex.recordingapp.retrofitclasses.models.apiRequest.OtpReq
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.OtpRes
import com.seekx.webService.ApiImp
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.otp_fragment.*

class OtpVerification : AppCompatActivity() {
    var editor: SharedPreferences.Editor? = null
    lateinit var apiImp: ApiImp
    var sharedpreferences: SharedPreferences? = null
    val MyPREFERENCES = "MyPrefs"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.otp_fragment)
        apiImp = ApiImp(this)
        setPrefernce()
        bt_submit.setOnClickListener {

            if (et_verification_code.text.toString().trim().length > 0) {
                verifyOtp(intent.getStringExtra("number")!!)
            } else {
                apiImp.dialogUtil.showAlert("Enter Otp")
            }

        }
        et_verification_code.setOnCompleteListener { value ->
            Toast.makeText(this, ""+value, Toast.LENGTH_SHORT).show()
           }

    }

    fun verifyOtp(number: String) {
        apiImp.showProgressBar()
        val loginreq = OtpReq(number, "match_otp_mobile_number", et_verification_code.text.toString())
        apiImp.verifyOtp(loginreq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                apiImp.cancelProgressBar()
                Log.e("TAG", "onSuccess: login="+status)
                val ss = any as OtpRes
                Log.e("TAG", "onSuccess: ss.interviewer_id="+ss.interviewer_id)

                if (status) {
                    editor?.putBoolean("islogin", true)?.commit()
                    editor?.putString("userid", ss.interviewer_id)?.commit()
                    editor?.putString("username", ss.name)?.commit()

                    intent = Intent(applicationContext, MainActivity::class.java)
                    intent.flags =  Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

                    startActivity(intent)
                } else {
                    apiImp.dialogUtil.showAlert(ss.msg)
                }

            }
        })


    }

    private fun setPrefernce() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
    }

}