package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class OtpReq(var mobile_no: String?, var service_name: String?, var otp: String?): BaseRequest()
