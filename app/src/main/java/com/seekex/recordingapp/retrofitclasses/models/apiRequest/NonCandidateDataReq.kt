package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class NonCandidateDataReq(var data: NonCandidateDataReq2?,var service_name: String): BaseRequest()
