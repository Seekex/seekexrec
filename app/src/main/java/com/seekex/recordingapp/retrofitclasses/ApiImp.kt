package com.seekx.webService

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.seekex.recordingapp.Contact
import com.seekex.recordingapp.CustomLoader
import com.seekex.recordingapp.DialogUtils
import com.seekex.recordingapp.R
import com.seekex.recordingapp.retrofitclasses.models.apiRequest.*
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.*
import com.seekex.scheduleproject.Preferences
import com.seekx.webService.ApiUtils.Companion.CHECKCANDIDATE

import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.BaseResponse
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File


open class ApiImp(val context: Context) {
    var editor: SharedPreferences.Editor? = null

    var pref: Preferences = Preferences(context)
    var dialogUtil: DialogUtils = DialogUtils(context)
    var customLoader: CustomLoader = CustomLoader(context)
    val MyPREFERENCES = "MyPrefs"


    companion object {
        const val TAG = "ApiImpResp"
    }


    private val webApi = ApiUtils.getWebApi()
    private val uploadFileApi = ApiUtils.getUploadApi()

    fun showProgressBar() {
        customLoader.show()
    }

    fun cancelProgressBar() {
        customLoader.cancel()
    }


    private fun onStartApiWithoutLoader(): Boolean {
        return checkInternet()
    }

    fun getListing(apiCallBack: ApiCallBack) {
        val reportReq = ListingReq("communication_dropdown_data")
        Log.e(TAG, "checkCanNumber: " + Gson().toJson(reportReq))

        ApiUtils.getWebApi().getListing(reportReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<GetListingRes> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: GetListingRes) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    fun hitDetailApi(number: SendDetailReq, apiCallBack: ApiCallBack) {
        Log.e(TAG, "checkCanNumber: " + Gson().toJson(number))

        ApiUtils.getWebApi().hitDetail(number).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: BaseResponse) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    fun checkCanNumber(number: String, apiCallBack: ApiCallBack) {
        val reportReq = CandidateReq(number, CHECKCANDIDATE, 1)
        Log.e(TAG, "checkCanNumber: " + Gson().toJson(reportReq))
        ApiUtils.getWebApi().checkCanNumber(reportReq).subscribeOn(Schedulers.io())

//        ApiUtils.getWebApi().checkCanNumber(reportReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<CheckNumberREs> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: CheckNumberREs) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }


    fun getDetails(number: String, url: String, apiCallBack: ApiCallBack) {
        val reportReq = GetDetailsReq(number, "send_recording_details")
        Log.e(TAG, "getDetails: " + Gson().toJson(reportReq))

        ApiUtils.getWebApi().getDetails(reportReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<GetDetailRes> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: GetDetailRes) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    fun login(number: LoginReq, apiCallBack: ApiCallBack) {
        Log.e(TAG, "checkCanNumber: " + Gson().toJson(number))

        ApiUtils.getWebApi().login(number).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LoginRes> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: LoginRes) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    fun verifyOtp(number: OtpReq, apiCallBack: ApiCallBack) {
        Log.e(TAG, "checkCanNumber: " + Gson().toJson(number))

        ApiUtils.getWebApi().verifyOtp(number).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<OtpRes> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: OtpRes) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    fun hitSingleFileDataApi(swdata: Contact, filepath: String, apiCallBack: ApiCallBack) {

        val mFile: RequestBody
        val reportReq = ReportReq(
            "save_call_recordings",
            filepath,
            swdata.call_datetime,
            swdata.phoneNumber,
            swdata.call_duration,
            swdata.candidate_id,
            "1",
            swdata.rec_number
        )
        Log.e("hitDataApi 11 ", "reportReq:i== " + Gson().toJson(reportReq))

        ApiUtils.getWebApi().uploadData(reportReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {}
                override fun onError(e: Throwable) {
                    Log.e("inbox api", "err: " + e.toString())

                    onResponseApi(e.toString(), apiCallBack)
                }

                override fun onNext(respnse: BaseResponse) {
                    Log.e("inbox api", "onNext: " + respnse.toString())
                    onResponseApi(respnse, apiCallBack)
                }
            })
    }


    fun hitSingleFileApi(swdata: Contact, apiCallBack: ApiCallBack) {

        val file = File(swdata.rec_file_path)
        val mFile: RequestBody
        mFile = RequestBody.create("application/pdf".toMediaTypeOrNull(), file)
        Log.e("hitApi 11 ", "uploadFile: " + file)
//        val reportReq = ReportReq(list.get(i).call_datetime, list.get(i).phoneNumber, list.get(i).rec_file_path)

        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)

//        val ss = ApiUtils.getUploadApi().uploadSingleFile(fileToUpload).execute()
//        if(ss.isSuccessful){
//            val response:GetRecordingsRes = ss.body()
//        }
        ApiUtils.getUploadApi().uploadSingleFile(fileToUpload)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<GetRecordingsRes> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    Log.e("upload error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: GetRecordingsRes) {
                    Log.e("upload res", "file: " + respnse)
                    Log.e("upload res", "file: " + respnse.data.file)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    fun hitApi(i: Int, list: ArrayList<Contact>, apiCallBack: ApiCallBack) {

        Log.e("hitApi 11 ", "uploadFile:i== " + i)
        val file = File(list.get(i).rec_file_path)
        val mFile: RequestBody
        mFile = RequestBody.create("application/pdf".toMediaTypeOrNull(), file)
        Log.e("hitApi 11 ", "uploadFile: " + file)
//        val reportReq = ReportReq(list.get(i).call_datetime, list.get(i).phoneNumber, list.get(i).rec_file_path)

        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi().uploadSingleFile(fileToUpload).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<GetRecordingsRes> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    Log.e("upload error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: GetRecordingsRes) {
                    Log.e("upload res", "file: " + respnse)
                    Log.e("upload res", "file: " + respnse.data.file)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    fun hitDataApi(i: Int, list: ArrayList<Contact>, filepath: String, apiCallBack: ApiCallBack) {

        Log.e("hitDataApi 11 ", "uploadFile:i== " + i)
        val mFile: RequestBody
        val reportReq = ReportReq(
            "save_call_recordings",
            filepath,
            list.get(i).call_datetime,
            list.get(i).phoneNumber,
            list.get(i).call_duration,
            list.get(i).candidate_id,
            "1",
            list.get(i).rec_number
        )
        Log.e("hitDataApi 11 ", "reportReq:i== " + Gson().toJson(reportReq))

        ApiUtils.getWebApi().uploadData(reportReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {}
                override fun onError(e: Throwable) {
                    Log.e("inbox api", "err: " + e.toString())

                    onResponseApi(e.toString(), apiCallBack)
                }

                override fun onNext(respnse: BaseResponse) {
                    Log.e("inbox api", "onNext: " + respnse.toString())
                    onResponseApi(respnse, apiCallBack)
                }
            })
    }


    fun hitNonCandidateDataApi(
        i: Int,
        list: ArrayList<Contact>,
        filepath: String,
        apiCallBack: ApiCallBack
    ) {

        Log.e("hitDataApi 11 ", "uploadFile:i== " + i)
        val mFile: RequestBody
        var sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
        val reportReq2 = NonCandidateDataReq2(list.get(i).phoneNumber,filepath,sharedpreferences.getString("userid",""), list.get(i).call_datetime,list.get(i).call_duration)


        val reportReq = NonCandidateDataReq(reportReq2, "save_calling_history")
        Log.e("hitDataApi 11 ", "reportReq:i== " + Gson().toJson(reportReq))

        ApiUtils.getWebApi().uploadNonCandidateData(reportReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {}
                override fun onError(e: Throwable) {
                    Log.e("inbox api", "err: " + e.toString())

                    onResponseApi(e.toString(), apiCallBack)
                }

                override fun onNext(respnse: BaseResponse) {
                    Log.e("inbox api", "onNext: " + respnse.toString())
                    onResponseApi(respnse, apiCallBack)
                }
            })
    }


    fun saveMissedCallData(
        i: Int,
        list: ArrayList<Contact>,
        apiCallBack: ApiCallBack
    ) {
        var sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
        Log.e("saveMissedCallData 11 ", "uploadFile:i== " + i)
        val mFile: RequestBody

        val reportReq2 = NonCandidateDataReq2(list.get(i).phoneNumber,"",sharedpreferences.getString("userid",""), list.get(i).call_datetime,"0")
        val reportReq = NonCandidateDataReq(reportReq2, "save_missed_call")
        Log.e("saveMissedCallData 11 ", "reportReq:i== " + Gson().toJson(reportReq))

        ApiUtils.getWebApi().uploadMisseddata(reportReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {}
                override fun onError(e: Throwable) {
                    Log.e("inbox api", "err: " + e.toString())

                    onResponseApi(e.toString(), apiCallBack)
                }

                override fun onNext(respnse: BaseResponse) {
                    Log.e("inbox api", "onNext: " + respnse.toString())
                    onResponseApi(respnse, apiCallBack)
                }
            })
    }


    private fun checkInternet(): Boolean {
//        if (!ExtraUtils.isNetworkConnectedMainThread(context)) {
//            dialogUtil.showAlert(context.getString(R.string.internet_error))
//            return false
//        }
        return true
    }

    private fun onStartApi(): Boolean {
        if (!checkInternet()) {
            dialogUtil.showAlert(context.getString(R.string.internet_error))
            return false
        }
//        customLoader.show()
        return true
    }


    private fun checkResMsg(msg: String): String {
        if (msg.contains("java.net") || msg.contains("retrofit2.adapter.rxjava")) {
//            toast(Preferences(context).get(AppConstants.token))
            return context.getString(R.string.connection_failed)
        }

        return msg
    }

    private fun onResponseApi(response: BaseResponse, Obj: Any?, apiCallBack: ApiCallBack) {
//        customLoader.cancel()
        customLoader.cancel()
        if (response.status != 1) {

            dialogUtil.showAlert(checkResMsg(response.msg))
            apiCallBack.onSuccess(false, response.msg)
        } else {
            apiCallBack.onSuccess(true, Obj!!)
        }
    }

    private fun onResponseApi(response: BaseResponse, apiCallBack: ApiCallBack?) {
        customLoader.cancel()
//        if (!response.status) {

//            if (onLogoutCheck(response))
//                return

//        }
        if (response.status == 1) {
            apiCallBack?.onSuccess(true, response)

        } else {
//            dialogUtil.showAlert(checkResMsg(response.msg))

            apiCallBack?.onSuccess(false, response)

        }

    }

    private fun onResponseApi(msg: String, apiCallBack: ApiCallBack?) {
//        customLoader.cancel()
        customLoader.cancel()
        Log.e("upload 44", "onResponseApi:msg= " + msg)

//        dialogUtil.showAlert(checkResMsg(msg))

        apiCallBack?.onSuccess(false, msg)

    }


}