package com.seekex.recordingapp.retrofitclasses.models.apiResponse

import com.seekx.webService.models.BaseResponse

data class GetMissedCallRes(var data: DataDTO): BaseResponse(0,"")