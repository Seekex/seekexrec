package com.seekex.recordingapp.retrofitclasses.models.apiResponse

import com.seekex.recordingapp.Contact
import com.seekx.webService.models.BaseResponse

data class GetListingRes(var conversation: ArrayList<ListingDTO>, var interest: ArrayList<ListingDTO>, var rating: ArrayList<ListingDTO>, var job_current_status: ArrayList<ListingDTO>, var remark_status: ArrayList<ListingDTO>): BaseResponse(0,"")