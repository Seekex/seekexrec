package com.seekx.webService

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RetrofitClient {


    companion object {

        val minTime=360L
        val maxTime=300L
//
        val fileUploadOkHttp = OkHttpClient.Builder()
            .connectTimeout(maxTime, TimeUnit.SECONDS)
            .writeTimeout(maxTime, TimeUnit.SECONDS)
            .readTimeout(maxTime, TimeUnit.SECONDS)
            .build()

        private val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(minTime, TimeUnit.SECONDS)
            .writeTimeout(minTime, TimeUnit.SECONDS)
            .readTimeout(minTime, TimeUnit.SECONDS)
            .build()

        private val gson = GsonBuilder()
            .setLenient()
            .create()

        var retrofit: Retrofit? = null

        fun getClient(baseUrl: String): Retrofit? {

            retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            return retrofit
        }

        fun getUploadFileClient(baseUrl: String): Retrofit? {


            retrofit = Retrofit.Builder()
                .client(fileUploadOkHttp)
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()


            return retrofit
        }
    }
}