package com.seekex.recordingapp.retrofitclasses.models.apiResponse

import com.seekx.webService.models.BaseResponse

data class OtpRes(var interviewer_id: String,var name: String): BaseResponse(0,"")