package com.seekx.webService

import com.seekx.webService.interfaces.ApiServices

class ApiUtils {

    companion object {
//        const val APIPREFIX = "api"
        const val APIPREFIX = "http://develop-erp.sevenrocks.in/ActivityMasters"
        const val API_HEADER = "Content-Type: application/json;charset=UTF-8"
        const val UPLOAD_REC_FILE = "https://stagging.sevenrocks.in/CronJobs/ajaxUploadTempFile"
        const val UPLOAD_DATA = "https://stagging.sevenrocks.in/web-api/"
        const val CHECKCANDIDATE = "check_candidate_exists"

        const val LOGIN = APIPREFIX + "/loginApi"

//        const val localUrl = "http://54.172.167.148:3333/"
        const val localUrl = "https://sevenrocks.in/"


        fun getWebApi(): ApiServices {
            return RetrofitClient.getClient(localUrl)!!.create(ApiServices::class.java)
        }
        fun getUploadApi(): ApiServices {
            return RetrofitClient.getUploadFileClient(localUrl)!!.create(ApiServices::class.java)
        }

    }

}