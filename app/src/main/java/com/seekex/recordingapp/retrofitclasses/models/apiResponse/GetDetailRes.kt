package com.seekex.recordingapp.retrofitclasses.models.apiResponse

import com.seekex.recordingapp.Contact
import com.seekx.webService.models.BaseResponse

data class GetDetailRes(var candidate_id:String,var candidate_name:String,var conversation_id:String,var conversation:String,var interest_id:String,var interest:String,var interviewer_id:String,var interviewer_name:String,var remarks:String,var candidate_rating:String,var candidate_number:String,var duration:String,var calltime:String,var remark_status_id:String,var remark_status:String,var candidate_status:String): BaseResponse(0,"")



