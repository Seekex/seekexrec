package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class NonCandidateDataReq2(var number: String?, var file: String?, var agent_id: String?, var calling_time: String?, var duration: String?): BaseRequest()
