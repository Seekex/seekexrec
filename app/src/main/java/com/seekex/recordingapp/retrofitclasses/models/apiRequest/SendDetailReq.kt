package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class SendDetailReq(var service_name: String?, var conversation_type_id: String?, var interest_id: String?, var remarks: String?, var candidate_rating: String?, var app_ref_id: String?, var interviewer_id: String?, var current_status: String?, var remark_status_id: String?): BaseRequest()
