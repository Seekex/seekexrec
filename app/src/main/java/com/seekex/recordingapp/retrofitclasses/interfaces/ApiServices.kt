package com.seekx.webService.interfaces

import com.seekex.recordingapp.retrofitclasses.models.apiRequest.*
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.*
import com.seekx.webService.ApiUtils
import com.seekx.webService.models.BaseResponse
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import rx.Observable


interface ApiServices {



    @Multipart
    @POST(ApiUtils.UPLOAD_REC_FILE)
    fun uploadSingleFile(@Part file: MultipartBody.Part?): Observable<GetRecordingsRes>
//
    @POST(ApiUtils.UPLOAD_DATA)
    fun uploadData(@Body reportReq: ReportReq): Observable<BaseResponse>

    @POST(ApiUtils.UPLOAD_DATA)
    fun uploadNonCandidateData(@Body reportReq: NonCandidateDataReq): Observable<BaseResponse>
    @POST(ApiUtils.UPLOAD_DATA)
    fun uploadMisseddata(@Body reportReq: NonCandidateDataReq): Observable<BaseResponse>

    @POST(ApiUtils.UPLOAD_DATA)
    fun checkCanNumber(@Body candidateReq: CandidateReq): Observable<CheckNumberREs>

    @POST(ApiUtils.UPLOAD_DATA)
    fun getDetails(@Body getdetail: GetDetailsReq): Observable<GetDetailRes>

    @POST(ApiUtils.UPLOAD_DATA)
    fun login(@Body loginReq: LoginReq): Observable<LoginRes>

    @POST(ApiUtils.UPLOAD_DATA)
    fun getListing(@Body listingReq: ListingReq): Observable<GetListingRes>

    @POST(ApiUtils.UPLOAD_DATA)
    fun hitDetail(@Body candidateReq: SendDetailReq): Observable<BaseResponse>

    @POST(ApiUtils.UPLOAD_DATA)
    fun verifyOtp(@Body loginReq: OtpReq): Observable<OtpRes>
//
//    @POST(ApiUtils.ACTIVITYTimer)
//    fun updateTimer(@Body activityReq: ActivityTimerReq): Observable<BaseResponse>

}