package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class CandidateReq(var number: String?,var service_name: String?,var is_test: Int?): BaseRequest()
