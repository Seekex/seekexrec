package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class ListingReq(var service_name: String?): BaseRequest()
