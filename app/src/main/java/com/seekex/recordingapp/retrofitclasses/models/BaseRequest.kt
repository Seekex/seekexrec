package com.seekx.webService.models

import androidx.room.Ignore


open  class BaseRequest(
                        var fcm: String,
                        var uid: Long){
    @Ignore
    constructor():this("",0L)
}