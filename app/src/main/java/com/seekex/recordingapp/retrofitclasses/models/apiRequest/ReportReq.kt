package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class ReportReq(var service_name: String?,var file: String?,var calldatetime: String?, var number: String?, var rec_duration: String?, var candidate_id: String?, var is_test: String?, var app_ref_id: String?): BaseRequest()
