package com.seekex.recordingapp.retrofitclasses.models.apiRequest

import com.seekx.webService.models.BaseRequest

data class GetDetailsReq(var app_ref_id: String?, var service_name: String?): BaseRequest()
