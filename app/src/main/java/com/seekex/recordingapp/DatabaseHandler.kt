package com.seekex.readstorage

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.seekex.recordingapp.Contact
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.ListingDTO


class DatabaseHandler(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    // Creating Tables  
    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_CONTACTS_TABLE = ("CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DATETIME + " TEXT," + KEY_NAME + " TEXT," + KEY_PH_DURATION + " TEXT," + KEY_REC_PATH + " TEXT," + KEY_STATUS + " TEXT," + KEY_PH_NO + " TEXT," + KEY_CAND_ID + " TEXT," + KEY_REC_NUMBER + " TEXT," + KEY_REC_URL + " TEXT," + KEY_CALL_TYPE + " TEXT," + KEY_CALL_DATE + " TEXT," + KEY_CALL_TIME + " TEXT" + ")")



        db.execSQL(CREATE_CONTACTS_TABLE)

        val CREATE_LISTING_TABLE = ("CREATE TABLE " + TABLE_LISTING + "("
                + KEY_ID_LISTING + " INTEGER PRIMARY KEY," + KEY_LISTINGID + " TEXT," + KEY_LISTINGVAL + " TEXT," + KEY_LISTINGTYPE + " TEXT" + ")")
        db.execSQL(CREATE_LISTING_TABLE)


        val CREATE_MISSED_CALL_TABLE = ("CREATE TABLE " + TABLE_MISSED + "("
                + KEY_MISSED_ID + " INTEGER PRIMARY KEY," + KEY_MISSED_PH_NO + " TEXT," + KEY_MISSED_CALL_TIME + " TEXT," + KEY_MISSED_STATUS + " TEXT" + ")")
        db.execSQL(CREATE_MISSED_CALL_TABLE)

    }

    // Upgrading database  
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS)

        // Create tables again  
        onCreate(db)
    }

    // code to add the new contact  
    fun addContact(date: String?, name: String?, number: String?, duration: String?, path: String?, status: String?, candidateid: String, recNumber: String, recurl: String, calltype: String, endtimecal: String, dateString: String) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_DATETIME, date) // Contact Name
        values.put(KEY_NAME, name) // Contact Name
        values.put(KEY_PH_NO, number) // Contact Phone
        values.put(KEY_PH_DURATION, duration) // Contact Phone
        values.put(KEY_REC_PATH, path) // Contact Phone
        values.put(KEY_STATUS, status) // Contact Phone
        values.put(KEY_CAND_ID, candidateid) // Contact Phone
        values.put(KEY_REC_NUMBER, recNumber) // Contact Phone
        values.put(KEY_REC_URL, recurl) // Contact Phone
        values.put(KEY_CALL_TYPE, calltype) // Contact Phone
        values.put(KEY_CALL_TIME, endtimecal) // Contact Phone
        values.put(KEY_CALL_DATE, dateString) // Contact Phone

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values)
        //2nd argument is String containing nullColumnHack
        Log.e("Database", "addContact:  date $date name $name number $number custid $candidateid recnumber $recNumber")
        db.close() // Closing database connection  
    }

    fun addMissedCalls(number: String?, calldatetime: String?, status: String?) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_MISSED_PH_NO, number) // Contact Phone
        values.put(KEY_MISSED_CALL_TIME, calldatetime) // Contact Name

        values.put(KEY_MISSED_STATUS, status) // Contact Phone


        // Inserting Row
        db.insert(TABLE_MISSED, null, values)
        //2nd argument is String containing nullColumnHack
        Log.e("Database", "addMissedCalls:  number $number calldatetime $calldatetime status $status")
        db.close() // Closing database connection
    }
    fun addListingTable(id: String?, vall: String?, type: String?) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_LISTINGID, id) // Contact Name
        values.put(KEY_LISTINGTYPE, type) // Contact Name
        values.put(KEY_LISTINGVAL, vall) // Contact Phone


        // Inserting Row
        try {
            db.insert(TABLE_LISTING, null, values)
        } catch (e: Exception) {
        }
        //2nd argument is String containing nullColumnHack
        Log.e("Database", "addListingTable:  id $id val $vall type $type")
        db.close() // Closing database connection
    }
    val allContacts: ArrayList<Contact>
        get() {
            val contactList = ArrayList<Contact>()
            // Select All Query
            val selectQuery = "SELECT  * FROM " + TABLE_CONTACTS
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)

            // looping through all rows and adding to list
            if (cursor.moveToLast()) {
                do {
                    val contact = Contact()
                    contact.setRec_file_path(cursor.getString(cursor.getColumnIndex(KEY_REC_PATH)))
                    contact.phoneNumber = cursor.getString(cursor.getColumnIndex(KEY_PH_NO))
                    contact.status = cursor.getString(cursor.getColumnIndex(KEY_STATUS))
                    contact.call_duration = cursor.getString(cursor.getColumnIndex(KEY_PH_DURATION))
                    contact.call_datetime = cursor.getString(cursor.getColumnIndex(KEY_DATETIME))
                    contact.candidate_id = cursor.getString(cursor.getColumnIndex(KEY_CAND_ID))
                    contact.rec_number = cursor.getString(cursor.getColumnIndex(KEY_REC_NUMBER))
                    contact.rec_url = cursor.getString(cursor.getColumnIndex(KEY_REC_URL))
                    contact.call_type = cursor.getString(cursor.getColumnIndex(KEY_CALL_TYPE))
                    contact.calldate = cursor.getString(cursor.getColumnIndex(KEY_CALL_DATE))
                    contact.calltime = cursor.getString(cursor.getColumnIndex(KEY_CALL_TIME))
                    // Adding contact to list
                    contactList.add(contact)
                } while (cursor.moveToPrevious())
            }

            // return contact list
            return contactList
        }
    val allMissedCalls: ArrayList<Contact>
        get() {
            val contactList = ArrayList<Contact>()
            // Select All Query
            val selectQuery = "SELECT  * FROM " + TABLE_MISSED
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)
            if (cursor.moveToLast()) {
                do {
                    val contact = Contact()
                    contact.phoneNumber = cursor.getString(cursor.getColumnIndex(KEY_MISSED_PH_NO))
                    contact.status = cursor.getString(cursor.getColumnIndex(KEY_MISSED_STATUS))
                    contact.call_datetime = cursor.getString(cursor.getColumnIndex(
                        KEY_MISSED_CALL_TIME))

                    // Adding contact to list
                    contactList.add(contact)
                } while (cursor.moveToPrevious())
            }
            return contactList
        }
    val allListings: ArrayList<ListingDTO>
        get() {
            val contactList = ArrayList<ListingDTO>()
            // Select All Query
            val selectQuery = "SELECT  * FROM " + TABLE_LISTING
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    val listingDTO = ListingDTO("")

                    listingDTO.id = (cursor.getString(cursor.getColumnIndex(KEY_LISTINGID)))
                    listingDTO.`val` = cursor.getString(cursor.getColumnIndex(KEY_LISTINGVAL))
                    listingDTO.type = cursor.getString(cursor.getColumnIndex(KEY_LISTINGTYPE))

                    // Adding contact to list
                    contactList.add(listingDTO)
                } while (cursor.moveToNext())
            }

            // return contact list
            return contactList
        }

    // code to get the single contact
    fun getContact(status: String): ArrayList<Contact> {
        val contactList = ArrayList<Contact>()
        val db = this.readableDatabase
//        val cursor = db.query(TABLE_CONTACTS, arrayOf(KEY_ID,
//                KEY_NAME, KEY_PH_NO, KEY_REC_PATH, KEY_PH_DURATION, KEY_DATETIME, KEY_CAND_ID, KEY_REC_NUMBER, KEY_REC_URL, KEY_CALL_TYPE, KEY_CALL_DATE, KEY_CALL_TIME), KEY_STATUS + " =? AND " + KEY_CAND_ID + " !=?", arrayOf(status, ""), null, null, null, null)

        val cursor = db.query(TABLE_CONTACTS, arrayOf(KEY_ID,
            KEY_NAME, KEY_PH_NO, KEY_REC_PATH, KEY_PH_DURATION, KEY_DATETIME, KEY_CAND_ID, KEY_REC_NUMBER, KEY_REC_URL, KEY_CALL_TYPE, KEY_CALL_DATE, KEY_CALL_TIME), KEY_STATUS + "=?" , arrayOf(status), null, null, null, null)

        if (cursor.moveToFirst()) {
            do {
                val contact = Contact()
                contact.id = cursor.getString(cursor.getColumnIndex(KEY_ID))
                contact.call_datetime = cursor.getString(cursor.getColumnIndex(KEY_DATETIME))
                contact.setRec_file_path(cursor.getString(cursor.getColumnIndex(KEY_REC_PATH)))
                contact.phoneNumber = cursor.getString(cursor.getColumnIndex(KEY_PH_NO))
                contact.call_duration = cursor.getString(cursor.getColumnIndex(KEY_PH_DURATION))
                contact.candidate_id = cursor.getString(cursor.getColumnIndex(KEY_CAND_ID))
                contact.rec_number = cursor.getString(cursor.getColumnIndex(KEY_REC_NUMBER))
                contact.rec_url = cursor.getString(cursor.getColumnIndex(KEY_REC_URL))
                contact.call_type = cursor.getString(cursor.getColumnIndex(KEY_CALL_TYPE))
                contact.calldate = cursor.getString(cursor.getColumnIndex(KEY_CALL_DATE))
                contact.calltime = cursor.getString(cursor.getColumnIndex(KEY_CALL_TIME))
                // Adding contact to list
                Log.e("TAG", "getContact: "+cursor.getString(cursor.getColumnIndex(KEY_ID)) )
                Log.e("TAG", "getContact: 22 "+contact.id )

                contactList.add(contact)


            } while (cursor.moveToNext())
        }

        // return contact list
        return contactList
    }
    fun getMissedData(status: String): ArrayList<Contact> {
        val contactList = ArrayList<Contact>()
        val db = this.readableDatabase
//        val cursor = db.query(TABLE_CONTACTS, arrayOf(KEY_ID,
//                KEY_NAME, KEY_PH_NO, KEY_REC_PATH, KEY_PH_DURATION, KEY_DATETIME, KEY_CAND_ID, KEY_REC_NUMBER, KEY_REC_URL, KEY_CALL_TYPE, KEY_CALL_DATE, KEY_CALL_TIME), KEY_STATUS + " =? AND " + KEY_CAND_ID + " !=?", arrayOf(status, ""), null, null, null, null)

        val cursor = db.query(
            TABLE_MISSED, arrayOf(
                KEY_MISSED_ID,
            KEY_MISSED_CALL_TIME, KEY_MISSED_PH_NO, KEY_MISSED_STATUS), KEY_MISSED_STATUS + "=?" , arrayOf(status), null, null, null, null)

        if (cursor.moveToFirst()) {
            do {
                val contact = Contact()
                contact.id = cursor.getString(cursor.getColumnIndex(KEY_MISSED_ID))
                contact.call_datetime = cursor.getString(cursor.getColumnIndex(KEY_MISSED_CALL_TIME))
                contact.phoneNumber = cursor.getString(cursor.getColumnIndex(KEY_MISSED_PH_NO))

                // Adding contact to list
                Log.e("TAG", "getContact: 22 "+contact.id )

                contactList.add(contact)


            } while (cursor.moveToNext())
        }

        // return contact list
        return contactList
    }
    fun getContactnew(status: String): ArrayList<Contact> {
        val contactList = ArrayList<Contact>()
        val db = this.readableDatabase
        val cursor = db.query(TABLE_CONTACTS, arrayOf(KEY_ID,
                KEY_NAME, KEY_PH_NO, KEY_REC_PATH, KEY_PH_DURATION, KEY_DATETIME, KEY_CAND_ID, KEY_REC_NUMBER, KEY_REC_URL, KEY_CALL_TYPE, KEY_CALL_TIME, KEY_CALL_DATE), KEY_STATUS + " =? AND " + KEY_CAND_ID + " =?", arrayOf(status, ""), null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                val contact = Contact()
                contact.id = cursor.getString(cursor.getColumnIndex(KEY_ID))
                contact.call_datetime = cursor.getString(cursor.getColumnIndex(KEY_DATETIME))
                contact.setRec_file_path(cursor.getString(cursor.getColumnIndex(KEY_REC_PATH)))
                contact.phoneNumber = cursor.getString(cursor.getColumnIndex(KEY_PH_NO))
                contact.call_duration = cursor.getString(cursor.getColumnIndex(KEY_PH_DURATION))
                contact.candidate_id = cursor.getString(cursor.getColumnIndex(KEY_CAND_ID))
                contact.rec_number = cursor.getString(cursor.getColumnIndex(KEY_REC_NUMBER))
                contact.rec_url = cursor.getString(cursor.getColumnIndex(KEY_REC_URL))
                contact.call_type = cursor.getString(cursor.getColumnIndex(KEY_CALL_TYPE))
                contact.calldate = cursor.getString(cursor.getColumnIndex(KEY_CALL_DATE))
                contact.calltime = cursor.getString(cursor.getColumnIndex(KEY_CALL_TIME))
                // Adding contact to list
                contactList.add(contact)
            } while (cursor.moveToNext())
        }

        // return contact list
        return contactList
    }

    fun isPathExists(path: String): Boolean {
        val db = this.readableDatabase
        var aa = false
        val cursor = db.query(TABLE_CONTACTS, arrayOf(KEY_STATUS), KEY_REC_PATH + "=?", arrayOf(path), null, null, null, null)
        if (cursor != null) {
            aa = cursor.count > 0
            cursor.close()
        }
        Log.e("database", "isPathExists: $aa")
        return aa
    }
    fun isDatetimeExists(datetime: String): Boolean {
        val db = this.readableDatabase
        var aa = false
        val cursor = db.query(TABLE_CONTACTS, arrayOf(KEY_DATETIME), KEY_DATETIME + "=?", arrayOf(datetime), null, null, null, null)
        if (cursor != null) {
            aa = cursor.count > 0
            cursor.close()
        }
        Log.e("database", "isPathExists: $aa")
        return aa
    }
    fun getStatus(path: String): String {
        val db = this.readableDatabase
        var aa = ""
        val cursor = db.query(TABLE_CONTACTS, arrayOf(KEY_STATUS), KEY_REC_PATH + "=?", arrayOf(path), null, null, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                aa = cursor.getString(0)
                Log.e("DatabaseHandler class", "getContact: " + cursor.getString(0))
            }
            cursor.close()
        }
        return aa
    }

    fun getNumberStatus(path: String?): String {
        val db = this.readableDatabase
        val c = db.rawQuery("SELECT * FROM TABLE_CONTACTS where KEY_REC_PATH=path", null)
        val name = c.getString(c.getColumnIndex(KEY_REC_PATH))
        db.close()
        return name
    }// Adding contact to list

    // return contact list  
// Select All Query  

    // looping through all rows and adding to list  
    // code to get all contacts in a list view


    // code to update the single contact  
    fun updateContact(status: String, recurl: String, id: String): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_STATUS, status)
        values.put(KEY_REC_URL, recurl)
        Log.e("TAG", "updateContact: ")
        // updating row  
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?", arrayOf(id))
    }
    fun updateMissedCallData(status: String, id: String): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_MISSED_STATUS, status)
        Log.e("TAG", "updateMissedCallData: ")
        // updating row
        return db.update(TABLE_MISSED, values, KEY_MISSED_ID+ " = ?", arrayOf(id))
    }

    fun updateContact(candId: String, id: String): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_CAND_ID, candId)
        Log.e("TAG", "updateContact: ")
        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?", arrayOf(id))
    }

    // Deleting single contact  
    fun deleteContact(contact: Contact) {
        val db = this.writableDatabase
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?", arrayOf(contact.id.toString()))
        db.close()
    }// return count  

    // Getting contacts Count  
    val contactsCount: Int
        get() {
            val countQuery = "SELECT  * FROM " + TABLE_CONTACTS
            val db = this.readableDatabase
            val cursor = db.rawQuery(countQuery, null)
            val ss = cursor.count
            cursor.close()

            // return count  
            return ss
        }

    companion object {
        private const val DATABASE_VERSION = 2
        private const val DATABASE_NAME = "contactsManager"
        private const val TABLE_CONTACTS = "contacts"
        private const val TABLE_LISTING = "listing"
        private const val TABLE_MISSED = "missed"

        private const val KEY_MISSED_ID = "id_miss"
        private const val KEY_MISSED_CALL_TIME = "call_time_miss"
        private const val KEY_MISSED_STATUS = "status_miss"
        private const val KEY_MISSED_PH_NO = "phone_number_miss"


        private const val KEY_ID = "id"
        private const val KEY_ID_LISTING = "id_listing"
        private const val KEY_NAME = "name"
        private const val KEY_PH_NO = "phone_number"
        private const val KEY_CAND_ID = "candidateid"
        private const val KEY_REC_NUMBER = "rec_number"
        private const val KEY_REC_URL = "rec_url"
        private const val KEY_CALL_TYPE = "call_type"
        private const val KEY_CALL_DATE = "call_date"
        private const val KEY_CALL_TIME = "call_time"
        private const val KEY_PH_DURATION = "phone_duration"
        private const val KEY_REC_PATH = "phone_path"
        private const val KEY_STATUS = "status"
        private const val KEY_DATETIME = "datetime"


        private const val KEY_LISTINGID = "listingid"
        private const val KEY_LISTINGVAL = "listingval"
        private const val KEY_LISTINGTYPE = "listingtype"


    }

    fun delete() {
        Log.e("TAG", "delete: database")
        val db = this.writableDatabase
        db.execSQL("delete from " + TABLE_CONTACTS)
        db.close()
    }
    fun deleteListingTable() {

            Log.e("TAG", "delete:listing  database")
            val db = this.writableDatabase
            db.execSQL("delete from " + TABLE_LISTING)
            db.close()
    }

}