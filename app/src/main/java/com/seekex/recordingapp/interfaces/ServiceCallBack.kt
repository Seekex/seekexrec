package com.seekx.interfaces

interface ServiceCallBack {
    fun onNextCall(count: Int?)
}