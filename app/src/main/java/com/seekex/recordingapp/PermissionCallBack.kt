package com.seekex.readstorage

interface PermissionCallBack {
    fun onPermissionAccessed()
}