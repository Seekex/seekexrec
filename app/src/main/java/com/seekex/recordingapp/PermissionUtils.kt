package com.seekex.readstorage

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AppOpsManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.provider.Settings
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat

import java.lang.reflect.Method


class PermissionUtils {

    companion object {

        @SuppressLint("ObsoleteSdkInt")
        @RequiresApi(Build.VERSION_CODES.M)


        fun requestOverlayDrawPermission(act: Activity, requestCode: Int) {
            val intent =
                Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + act.packageName)
                )
            act.startActivityForResult(intent, requestCode)
        }

        @JvmField
        val callingPermission = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.MODIFY_AUDIO_SETTINGS,
            Manifest.permission.RECORD_AUDIO
        )

        val introPermissionPermission = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
        )

        val storagePermission = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_CALL_LOG
        )

        val windowPermission = arrayOf(
            Manifest.permission.SYSTEM_ALERT_WINDOW
        )

       /* fun askAccessibilityPermission(context: Context, pref: Preferences): Boolean {

            if (ActivityUtils.isAccessibilityServiceEnabled(
                    context,
                    RemoteRecordService::class.java
                )
            ) {
                pref.setBoolean(AppConstants.isAccessbilityEnabled, true).commit()
            } else {
                val intent: Intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
                context.startActivity(intent)
            }

            return false


        }*/

        @JvmStatic
        fun checkStoragePermission(
            permissionCallBack: PermissionCallBack,
            activity: Activity,
            permission: Array<String>
        ): PermissionCallBack {

            if (Build.VERSION.SDK_INT > 22) {
                ActivityCompat.requestPermissions(
                    activity,
                    permission,
                    0
                )
            } else
                permissionCallBack.onPermissionAccessed()

            return permissionCallBack
        }


    }
}