package com.seekex.recordingapp

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import java.lang.Exception

class CustomAdapter(private val dataSet: ArrayList<Contact>, private val context: Context, var test: ((ss: String, duration: String, rec_url: String) -> Unit)) :
        RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        val tvName: TextView
        val tvdatetime: TextView
        val tvduration: TextView
        val details: ImageView
        val status: ImageView
        val calltype: ImageView
        val ll: LinearLayout
        val ll2: LinearLayout

        init {
            // Define click listener for the ViewHolder's View.
            tvName = view.findViewById(R.id.txtnumber)
            tvdatetime = view.findViewById(R.id.txtdatetime)
            tvduration = view.findViewById(R.id.txtduration)
            details = view.findViewById(R.id.details)
            status = view.findViewById(R.id.status)
            calltype = view.findViewById(R.id.img_calltype)
            ll = view.findViewById(R.id.ll_detail)
            ll2 = view.findViewById(R.id.ll_detail2)
        }
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.storage_adapter, viewGroup, false)

        return ViewHolder(view)
    }

    private fun getDurationString(seconds: Int): String? {
        var seconds = seconds
        val hours = seconds / 3600
        val minutes = seconds % 3600 / 60
        seconds = seconds % 60
        return twoDigitString(hours) + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds)
    }

    private fun twoDigitString(number: Int): String {
        if (number == 0) {
            return "00"
        }
        return if (number / 10 == 0) {
            "0$number"
        } else number.toString()
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        Log.e("adapter", "onBindViewHolder: " + dataSet.get(position).phoneNumber)
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.tvName.text = dataSet.get(position).phoneNumber
//        viewHolder.tvRec.text = dataSet.get(position).getRec_file_path()
        viewHolder.tvdatetime.text = dataSet.get(position).getCalltime()+" "+dataSet.get(position).getCalldate()

        if (dataSet.get(position).getStatus().equals("0")) {
            viewHolder.status.visibility = View.VISIBLE
        } else {
            viewHolder.status.visibility = View.GONE
        }



        Log.e("TAG", "onBindViewHolder: "+dataSet.get(position).getCall_type() )
        if (dataSet.get(position).getCall_type().equals("1")) {
            viewHolder.calltype.setBackgroundResource(R.drawable.call_incoming)
            viewHolder.details.visibility=View.VISIBLE
        } else  if (dataSet.get(position).getCall_type().equals("2")) {
            viewHolder.calltype.setBackgroundResource(R.drawable.call_outgoing)
            viewHolder.details.visibility=View.VISIBLE
        }
        else  if (dataSet.get(position).getCall_type().equals("3")) {
            viewHolder.calltype.setBackgroundResource(android.R.drawable.sym_call_missed)
            viewHolder.details.visibility=View.GONE
        }
        val index: Int = dataSet.get(position).getCall_duration().indexOf(" ")

        try {
            val firstString: String = dataSet.get(position).getCall_duration().substring(0, index)
            viewHolder.tvduration.text = getDurationString(firstString.toInt())// dataSet.get(position).getCall_duration()

        }catch (e:Exception){

        }

//        viewHolder.switch.setOnCheckedChangeListener{ compoundButton: CompoundButton, b: Boolean ->
//
//            swdata.invoke(dataSet.get(position))

//        }


        viewHolder.details.setOnClickListener {
            if (dataSet.get(position).getStatus().equals("0")) {
                test.invoke(dataSet.get(position).getRec_number(), dataSet.get(position).getCall_duration(), dataSet.get(position).getRec_url())
            }
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}
