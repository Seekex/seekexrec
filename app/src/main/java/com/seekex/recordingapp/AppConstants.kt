package com.seekex.scheduleproject

import android.os.Environment

object AppConstants {


    const val SPACES =" "
    const val SPACES_DOTS =" . "
    const val isNull="null"

    const val EMPTY=""
    const val NA="NA"
    const val uid="uid"
    const val isLogin="isLogin"
    const val SPININTEREST="interest"
    const val SPINREMARK_STATUS="remark_status"
    const val SPINSTATUS_LIST="status_list"
    const val SPINCON_TYPE="con_type"
    const val SPINRATING_LIST="rating_list"
    const val pic_url="pic_url"


}