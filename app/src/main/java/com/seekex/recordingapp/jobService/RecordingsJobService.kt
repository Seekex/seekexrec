package com.seekex.readstorage.jobService

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.SharedPreferences
import android.database.Cursor
import android.media.AudioManager
import android.os.Build
import android.provider.CallLog
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.seekex.readstorage.DatabaseHandler
import com.seekex.recordingapp.Contact
import com.seekex.recordingapp.R
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.CheckNumberREs
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.GetRecordingsRes
import com.seekx.webService.ApiImp
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.lang.Long
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileTime
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


//    The JobService runs on the main thread so any logic needs to perform, should be in a separate thread.
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
class RecordingsJobService : JobService() {
    lateinit var db: DatabaseHandler
    val MyPREFERENCES = "MyPrefs"
    var sharedpreferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null
    var batchSize = 10
    var start = 0
    var end = batchSize
    var call_Counter = 0
    lateinit var apiImp: ApiImp
    var list: ArrayList<Contact> = java.util.ArrayList()
    private var missed_call_Counter = 0
    private var missedList: ArrayList<Contact> = java.util.ArrayList()


    override fun onCreate() {
        super.onCreate()
        Log.e(TAG, "JobService created.")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e(TAG, "JobService destroyed.")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartJob(params: JobParameters): Boolean {
        Log.e(TAG, "on start job")
        Log.e(TAG, "onStartJob: " + isCallActive())
        db = DatabaseHandler(this)
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
        apiImp = ApiImp(this)

        var fileLocation = sharedpreferences!!.getString("folderLocation", "")
        try {
            saveMissedCalldetails()
        } catch (e: Exception) {
        }
        if (fileLocation!!.length > 0) {

            Log.e(TAG, "onStartJob: fileLocation $fileLocation")
            getRecFiles(fileLocation!!)
            uploadData()
//            checkPendingRecforCandId()
        } else {
            Toast.makeText(applicationContext, "Storage Path not picked.!", Toast.LENGTH_SHORT)
                .show()

        }

        // runClockOutApi(params);

        // return false, if job is finished.
        return true
    }

//    fun checkPendingRecforCandId() {
//        Log.e(TAG, "path exist false: ")
//        var alist = db.getContactnew("1")
//        Log.e(TAG, "path exist size: " + alist.size)
//
//        for (i in alist.indices) {
//            checkifExists(alist.get(i).phoneNumber, alist.get(i).id)
//        }
//    }

    fun checkifExists(phNumber: String, id: String) {
        Log.e(TAG, "checkifExists: $phNumber id $id")
        apiImp.checkCanNumber(phNumber, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e(TAG, "onSuccess: filepath=")
                val ss = any as CheckNumberREs
                if (ss.success == 1) {
                    db.updateContact(ss.candidate_id, id)
                    apiImp.hitApi(call_Counter, list, object : ApiCallBack {
                        override fun onSuccess(status: Boolean, any: Any) {
                            Log.e("TAG", "onSuccess: filepath=")

                            if (status) {
                                var ss = any as GetRecordingsRes
                                var filepath = ss.data.path + ss.data.filename
                                Log.e("TAG", "onSuccess: filepath=$filepath")
                                apiImp.hitDataApi(
                                    call_Counter,
                                    list,
                                    filepath,
                                    object : ApiCallBack {
                                        override fun onSuccess(status: Boolean, any: Any) {
                                            if (status) {

                                                db.updateContact(
                                                    "0",
                                                    filepath,
                                                    list.get(call_Counter).id
                                                )
                                                if (call_Counter < list.size) {
                                                    call_Counter++
                                                    uploadFileNew()
                                                }
                                            }
                                        }
                                    })
                            }
                        }
                    })
                } else {
                    apiImp.hitApi(call_Counter, list, object : ApiCallBack {
                        override fun onSuccess(status: Boolean, any: Any) {
                            Log.e("TAG", "onSuccess: filepath=")

                            if (status) {
                                var ss = any as GetRecordingsRes
                                var filepath = ss.data.path + ss.data.filename
                                Log.e("TAG", "onSuccess: filepath=$filepath")
                                apiImp.hitNonCandidateDataApi(
                                    call_Counter,
                                    list,
                                    filepath,
                                    object : ApiCallBack {
                                        override fun onSuccess(status: Boolean, any: Any) {
                                            if (status) {
                                                Log.e(
                                                    "TAG",
                                                    "onSuccess 11: filepath=${list.get(call_Counter).rec_file_path}"
                                                )

                                                db.updateContact(
                                                    "0",
                                                    filepath,
                                                    list.get(call_Counter).id
                                                )
                                                if (call_Counter < list.size) {
                                                    call_Counter++
                                                    uploadFileNew()
                                                }
                                            }
                                        }
                                    })
                            }
                        }
                    })
                }
            }
        })
    }

    private fun uploadMissedCallData() {
        missed_call_Counter = 0
        missedList.clear()
        missedList = db.getContact("missed")
        Log.e(TAG, "uploadMissedCallData: " + missedList.size)
        if (missedList.size == 0) {
//            Toast.makeText(applicationContext, "No New Record to Upload!", Toast.LENGTH_SHORT).show()
            return
        }
        uploadMissCallData()
    }

    private fun uploadData() {
        Log.e(TAG, "uploadData: ")
        call_Counter = 0
        list.clear()
        list = db.getContact("1")
        Log.e(TAG, "uploadData:size " + list.size)
        if (list.size == 0) {
            Toast.makeText(applicationContext, "No New Record to Upload!", Toast.LENGTH_SHORT)
                .show()
            uploadMissedCallData()
            return
        }
        uploadFileNew()
    }

    fun isCallActive(): Boolean {
        val manager = getSystemService(AUDIO_SERVICE) as AudioManager
        return manager.mode == AudioManager.MODE_IN_CALL
    }

    override fun onStopJob(params: JobParameters): Boolean {
        Log.e(TAG, "onStopJob - job cancelled before completion.")
//        Toast.makeText(applicationContext, "Job Stopped!", Toast.LENGTH_SHORT).show()

        // return true, if wants to re-run/re-schedule
        return true
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getRecFiles(folderLocation: String) {
        var pp: String = folderLocation
        val file = File(pp)
        val files = file.listFiles()// get all files from folder
        try {
            Log.e(TAG, " file Size: " + files.size)
            if (files.size == 0) {
//                Toast.makeText(this, "Selected folder is empty", Toast.LENGTH_LONG).show()
                return
            }
        } catch (e: Exception) {
        }
        try {
            Arrays.sort(files, Comparator.comparingLong(File::lastModified).reversed());
        } catch (e: Exception) {
        }

        val fileList = ArrayList<File>()
        var tb_count: Int = db.contactsCount
        try {
            for (i in files.indices) {
                val filePath: Path = files[i].toPath()
                var attr: BasicFileAttributes? = null

                attr = Files.readAttributes(filePath, BasicFileAttributes::class.java)
                val creationTime: FileTime = attr.creationTime()
                var installTime = sharedpreferences!!.getLong("installtime", 0)
                val creationTimeMillis = creationTime.toMillis()
                // filter all files and add in new list (cretiontime >installtime and file is not yet added to db)

                if (creationTimeMillis > installTime) {
                    if (tb_count == 0) {
                        fileList.add(files[i])
                    } else {
                        if (!db.isPathExists(files[i].absolutePath.toString())) {
                            fileList.add(files[i])
                        }
                    }
                    Log.e(TAG, " fileList size: " + fileList.size + " filepath " + files[i])
                }
            }
        } catch (e: Exception) {
        }
        for (i in fileList.indices) {

            val filePath: Path = fileList[i].toPath()
            var attr: BasicFileAttributes? = null

            attr = Files.readAttributes(filePath, BasicFileAttributes::class.java)
            val creationTime: FileTime = attr.creationTime()
            val df = SimpleDateFormat("HH:mm")
            val df2 = SimpleDateFormat("dd-MM-yyyy")
            val timeCreated = df.format(creationTime.toMillis())
            val dateCreated = df2.format(creationTime.toMillis())

            Log.e(
                TAG,
                " Filepath:" + fileList[i].absolutePath + " dateCreated:" + dateCreated + " creationTime:" + timeCreated
            )
            if (!db.isPathExists(fileList[i].absolutePath.toString())) {
                getCallDetails(
                    files.size,
                    db,
                    dateCreated,
                    timeCreated,
                    fileList[i].absolutePath.toString(),
                    creationTime.toMillis().toLong()
                )
            }
        }
    }


    private fun getCallDetails(
        filesize: Int,
        db: DatabaseHandler,
        recDateCreated: String,
        recTimeCreated: String,
        absolutePath: String,
        rec_time_millis: kotlin.Long
    ) {
        val projection = arrayOf(
            CallLog.Calls.CACHED_NAME,
            CallLog.Calls.NUMBER,
            CallLog.Calls.TYPE,
            CallLog.Calls.DATE,
            CallLog.Calls.DURATION
        )

        var fromtime = sharedpreferences!!.getLong("installtime", 0)
        val formatter2 = SimpleDateFormat("dd-MM-yyyy")
        val dateString: String = formatter2.format(Date(fromtime))
        Log.e("dateString", "getCallDetails: $dateString")


        val managedCursor: Cursor? = applicationContext.contentResolver.query(
            CallLog.Calls.CONTENT_URI, arrayOf(
                CallLog.Calls.TYPE, CallLog.Calls.DATE, CallLog.Calls.DURATION,
                CallLog.Calls.NUMBER, CallLog.Calls._ID
            ),
            CallLog.Calls.DATE + ">?", arrayOf(java.lang.String.valueOf(fromtime)),
            CallLog.Calls.NUMBER + " asc"
        )


        val number = managedCursor?.getColumnIndex(CallLog.Calls.NUMBER)
        val type = managedCursor?.getColumnIndex(CallLog.Calls.TYPE)
        val date = managedCursor?.getColumnIndex(CallLog.Calls.DATE)
        val duration = managedCursor?.getColumnIndex(CallLog.Calls.DURATION)
//        val name = managedCursor?.getColumnIndex(CallLog.Calls.CACHED_NAME)

        while (managedCursor!!.moveToNext()) {


            val phNumber = managedCursor.getString(number!!)
//            val phname = managedCursor.getString(name!!)
            val callType = managedCursor.getString(type!!)
            val callDate = managedCursor.getString(date!!)
            val callDayTime = Date(Long.valueOf(callDate))
            val callDuration = managedCursor.getString(duration!!)
            var dir: String = ""
            val dircode: Int = callType!!.toInt()
            when (dircode) {
                CallLog.Calls.OUTGOING_TYPE -> dir = "OUTGOING"
                CallLog.Calls.INCOMING_TYPE -> dir = "INCOMING"
                CallLog.Calls.MISSED_TYPE -> dir = "MISSED"
            }

            Log.e("calldetails", ": $phNumber")

            var calldurLng = callDuration.toLong()
            val calldurationInMillis =
                calldurLng * 1000 // adding millis to call duration for calculation

            val formatCalldate = callDate.toLong()// call datetime in mills
            var totalCallTime = formatCalldate + calldurationInMillis// call duration + call time
            var diff = rec_time_millis - totalCallTime
            val formatter = SimpleDateFormat("HH:mm")
            val formatter2 = SimpleDateFormat("dd-MM-yyyy")
            val formatter3 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val timeString: String = formatter.format(Date(formatCalldate))
            val dateString: String = formatter2.format(Date(formatCalldate))
            val datetimeforapi: String = formatter3.format(Date(formatCalldate))
            val endtimecal: String = formatter.format(totalCallTime)
            val tomillisecal: String = formatter.format(rec_time_millis)
            var recNumber = getRecordingNumber()

            if (endtimecal.equals(tomillisecal) && callDuration.toLong() > 0 && recDateCreated.equals(
                    dateString
                )
            ) {
                db.addContact(
                    datetimeforapi,
                    "",
                    phNumber,
                    callDuration + " sec",
                    absolutePath,
                    "1",
                    "",
                    recNumber.toString(),
                    "",
                    dircode.toString(),
                    endtimecal,
                    dateString
                )
            } else {
                if ((diff > 0 && diff < 20000) && callDuration.toLong() > 0 && recDateCreated.equals(
                        dateString
                    )
                ) {

                    db.addContact(
                        datetimeforapi,
                        "",
                        phNumber,
                        callDuration + " sec",
                        absolutePath,
                        "1",
                        "",
                        recNumber.toString(),
                        "",
                        dircode.toString(),
                        endtimecal,
                        dateString
                    )


                }
            }
        }

        managedCursor.close()
    }

    fun checkCanIdExists(
        phNumber: String,
        datetimeforapi: String,
        phname: String,
        callDuration: String,
        absolutePath: String,
        statuss: String,
        recnumber: String,
        recurl: String,
        dir: String,
        endtimecal: String,
        dateString: String
    ) {
        apiImp.checkCanNumber(phNumber, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e(TAG, "onSuccess: filepath=")
                val ss = any as CheckNumberREs
                if (ss.success == 1) {
                    db.addContact(
                        datetimeforapi,
                        phname,
                        phNumber,
                        callDuration,
                        absolutePath,
                        statuss,
                        ss.candidate_id,
                        recnumber,
                        recurl,
                        dir,
                        endtimecal,
                        dateString
                    )
                    return
                } else {
                    db.addContact(
                        datetimeforapi,
                        phname,
                        phNumber,
                        callDuration,
                        absolutePath,
                        statuss,
                        "",
                        recnumber,
                        recurl,
                        dir,
                        endtimecal,
                        dateString
                    )
                    return
                }
            }
        })
    }

    private fun getRecordingNumber(): Int {
        val rand = Random()
        val abcd = rand.nextInt(100)
        return abcd
    }

    private fun saveMissedCalldetails() {

        var fromtime = sharedpreferences!!.getLong("installtime", 0)
        val formatter2 = SimpleDateFormat("dd-MM-yyyy")
        val dateString: String = formatter2.format(Date(fromtime))
        Log.e("dateString", "getCallDetails: $dateString")


        val managedCursor: Cursor? = applicationContext.contentResolver.query(
            CallLog.Calls.CONTENT_URI, arrayOf(
                CallLog.Calls.TYPE, CallLog.Calls.DATE, CallLog.Calls.DURATION,
                CallLog.Calls.NUMBER, CallLog.Calls._ID
            ),
            CallLog.Calls.DATE + ">?", arrayOf(java.lang.String.valueOf(fromtime)),
            CallLog.Calls.NUMBER + " asc"
        )


        val number = managedCursor?.getColumnIndex(CallLog.Calls.NUMBER)
        val type = managedCursor?.getColumnIndex(CallLog.Calls.TYPE)
        val date = managedCursor?.getColumnIndex(CallLog.Calls.DATE)
        val duration = managedCursor?.getColumnIndex(CallLog.Calls.DURATION)
//        val name = managedCursor?.getColumnIndex(CallLog.Calls.CACHED_NAME)

        while (managedCursor!!.moveToNext()) {


            val phNumber = managedCursor.getString(number!!)
//            val phname = managedCursor.getString(name!!)
            val callType = managedCursor.getString(type!!)
            val callDate = managedCursor.getString(date!!)
            val callDayTime = Date(Long.valueOf(callDate))
            val callDuration = managedCursor.getString(duration!!)
            var dir: String = ""
            val dircode: Int = callType!!.toInt()
            when (dircode) {
                CallLog.Calls.OUTGOING_TYPE -> dir = "OUTGOING"
                CallLog.Calls.INCOMING_TYPE -> dir = "INCOMING"
                CallLog.Calls.MISSED_TYPE -> dir = "MISSED"
            }

            Log.e("calldetails", ": $phNumber")
            Log.e("duration", ": $callDuration")

            var calldurLng = callDuration.toLong()
            val calldurationInMillis =
                calldurLng * 1000 // adding millis to call duration for calculation

            val formatCalldate = callDate.toLong()// call datetime in mills
            var totalCallTime = formatCalldate + calldurationInMillis// call duration + call time
            val formatter = SimpleDateFormat("HH:mm")
            val formatter2 = SimpleDateFormat("dd-MM-yyyy")
            val formatter3 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val timeString: String = formatter.format(Date(formatCalldate))
            val dateString: String = formatter2.format(Date(formatCalldate))
            val datetimeforapi: String = formatter3.format(Date(formatCalldate))

            val endtimecal: String = formatter.format(totalCallTime)

            if (dir.equals("MISSED") || callDuration.equals("0")) {
                Log.e(TAG, "saveMissedCalldetails: " + datetimeforapi)
                if (!db.isDatetimeExists(datetimeforapi)) {
                    db.addContact(
                        datetimeforapi,
                        "",
                        phNumber,
                        "0 sec",
                        "",
                        "missed",
                        "",
                        "",
                        "",
                        dircode.toString(),
                        endtimecal,
                        dateString
                    )
                }
            }

        }
        managedCursor.close()
    }

    private fun uploadFileNew() {
        Log.e(TAG, "uploadFileNew: call_Counter = " + call_Counter)

        checkifExists(list.get(call_Counter).phoneNumber, list.get(call_Counter).id)
    }


//    private fun hitApi(i: Int, list: ArrayList<Contact>) {
//        val file = File(list.get(i).rec_file_path)
//        val mFile: RequestBody
//        mFile = RequestBody.create(MediaType.parse("application/pdf"), file)
//        Log.e("chatfile 11 ", "uploadFile: " + file)
//        val reportReq = ReportReq(list.get(i).call_datetime, list.get(i).phoneNumber, list.get(i).rec_file_path)
//
//        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
//        ApiUtils.getUploadApi().uploadSingleFile(fileToUpload).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(object : Observer<BaseResponse> {
//                    override fun onCompleted() {}
//
//                    override fun onError(e: Throwable) {
//                        Log.e("chatfile error ", "file: " + e.printStackTrace())
//                        Log.e("chatfile error ", "file: " + e.toString())
////                        apiImp.cancelProgressBar()
////                        rr_chat.visibility = View.VISIBLE
//                    }
//
//                    override fun onNext(respnse: BaseResponse) {
//                        Log.e("upload res", "file: " + respnse)
//                        db.updateContact("0", list.get(i).id)
//                        if (call_Counter<list.size){
//                            uploadFileNew(call_Counter++)
//
//                        }
//
//
//
//
//                    }
//                })
//    }

    /*
    private void getClassesAsync(final JobParameters params) {
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        String currentDateTime = DateTimeUtility.getFormattedDateTimeSlashFormatSansT(timeInMillis);

        UserServices mUserServices = new UserServicesImpl();
        mUserServices.getTodayClassesByInstructor(
                7,
                10824,
                "04/26/2021 13:00:39", //currentDateTime,
                new ResponseCallBackHandler<ArrayList<ScheduleModel>>() {
                    @Override
                    public void returnResponse(ResponseHandler<ArrayList<ScheduleModel>> responseHandler) {
                        AppLogger.Logger.info(TAG, "API response received.");
                        if (responseHandler.isExecuted()) {
                            ArrayList<ScheduleModel> models = responseHandler.getValue();
//                            updateList(models);
                        } else {
//                            onError(responseHandler.getMessage());
//                            setRecyclerViewVisibility();
                        }
                        makeNotification(ClockOutJobService.this);
                        jobFinished(params, false);
                        AppLogger.Logger.info(TAG, "Job finished.");
                        Toast.makeText(getApplicationContext(), "Job finished.!", Toast.LENGTH_SHORT).show();

                    }
                });
    }
*/
    private fun runClockOutApi(params: JobParameters) {
//        long timeInMillis = Calendar.getInstance().getTimeInMillis();
//        String currentDateTime = DateTimeUtility.getFormattedDateTimeSlashFormatSansT(timeInMillis);
//
//        UserSession mUserSession = MyApplication.getMyApplication().getUserSession();
//        LocationModel locationModel = mUserSession.getLocation();
//
//        InstructorClockOutDto dto = new InstructorClockOutDto();
//        dto.setProgramClassId(mClockedInClass.getProgramClassId());
//        dto.setLocation(locationModel.getLocation());
//        dto.setLongitude(locationModel.getLongitude());
//        dto.setLatitude(locationModel.getLatitude());
//        dto.setNote("");
//        dto.setCurrentDate(currentDateTime);
//
//        showLoading();
//      UserServices mUserServices = new UserServicesImpl();
//        mUserServices.clockOutInstructor(dto, new ResponseCallBackHandler() {
//            @Override
//            public void returnResponse(ResponseHandler responseHandler) {
//                hideLoading();
//                if (responseHandler.isExecuted()) {
//                    getClockedInSchedule();
//                } else {
//                    showInformationDialog(responseHandler.getMessage());
//                }
//            }
//        });
    }

    private fun makeNotification(context: Context) {
        createNotificationChannel()
        val builder = NotificationCompat.Builder(context, "notifyLe")
            .setSmallIcon(R.drawable.ic_folder_icon)
            .setContentTitle("Class Clock Out!")
            .setContentText("Your class has finished.")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        val notificationManagerCompat = NotificationManagerCompat.from(context)
        notificationManagerCompat.notify(200, builder.build())
    }

    private fun uploadMissCallData() {
        Log.e("TAG", " missed_call_Counter = " + missed_call_Counter)
        Log.e("TAG", " list size = " + missedList.size)

        apiImp.saveMissedCallData(missed_call_Counter, missedList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e("TAG", "onSuccess: filepath=")

                if (status) {
                    db.updateContact("0", "", missedList.get(missed_call_Counter).id)

                    if (missed_call_Counter < missedList.size - 1) {
                        missed_call_Counter++
                        uploadMissCallData()
                    }
                }
            }

        })
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "CLockoutName"
            val description = "Channel for ClockOut"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("notifyLe", name, importance)
            channel.description = description
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        private val TAG = "my Service"
    }
}