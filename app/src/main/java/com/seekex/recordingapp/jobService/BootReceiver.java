package com.seekex.recordingapp.jobService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.seekex.recordingapp.activityies.MainActivity;


public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = BootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context.getApplicationContext(), "Boot onReceive: ...", Toast.LENGTH_SHORT).show();
        Intent intent1 = new Intent(context.getApplicationContext(), MainActivity.class);
        context.startActivity(intent1);
//        ChangeActivityHelper.changeActivity(context.getApplicationContext(), DashboardInstructorActivity.class, false);
    }
}
