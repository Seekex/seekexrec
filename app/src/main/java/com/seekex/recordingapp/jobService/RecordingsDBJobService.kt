package com.seekex.readstorage.jobService

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.SharedPreferences
import android.database.Cursor
import android.media.AudioManager
import android.os.Build
import android.provider.CallLog
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.seekex.readstorage.DatabaseHandler
import com.seekex.recordingapp.Contact
import com.seekex.recordingapp.R
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.CheckNumberREs
import com.seekex.recordingapp.retrofitclasses.models.apiResponse.GetRecordingsRes
import com.seekx.webService.ApiImp
import com.seekx.webService.interfaces.ApiCallBack
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileTime
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


//    The JobService runs on the main thread so any logic needs to perform, should be in a separate thread.
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
class RecordingsDBJobService : JobService() {
    lateinit var db: DatabaseHandler
    val MyPREFERENCES = "MyPrefs"
    var sharedpreferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null

    override fun onCreate() {
        super.onCreate()
        Log.e(TAG, "JobService created.")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e(TAG, "JobService destroyed.")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartJob(params: JobParameters): Boolean {
        Log.e(TAG, "on dbjob start job")
        db = DatabaseHandler(this)
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
        editor?.putLong("installtime", System.currentTimeMillis())?.commit()
//        db.delete()


        // return false, if job is finished.
        return true
    }



    override fun onStopJob(params: JobParameters): Boolean {
        Log.e(TAG, "onStopJob -db job cancelled before completion.")
//        Toast.makeText(applicationContext, "Job Stopped!", Toast.LENGTH_SHORT).show()

        // return true, if wants to re-run/re-schedule
        return true
    }



//    private fun hitApi(i: Int, list: ArrayList<Contact>) {
//        val file = File(list.get(i).rec_file_path)
//        val mFile: RequestBody
//        mFile = RequestBody.create(MediaType.parse("application/pdf"), file)
//        Log.e("chatfile 11 ", "uploadFile: " + file)
//        val reportReq = ReportReq(list.get(i).call_datetime, list.get(i).phoneNumber, list.get(i).rec_file_path)
//
//        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
//        ApiUtils.getUploadApi().uploadSingleFile(fileToUpload).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(object : Observer<BaseResponse> {
//                    override fun onCompleted() {}
//
//                    override fun onError(e: Throwable) {
//                        Log.e("chatfile error ", "file: " + e.printStackTrace())
//                        Log.e("chatfile error ", "file: " + e.toString())
////                        apiImp.cancelProgressBar()
////                        rr_chat.visibility = View.VISIBLE
//                    }
//
//                    override fun onNext(respnse: BaseResponse) {
//                        Log.e("upload res", "file: " + respnse)
//                        db.updateContact("0", list.get(i).id)
//                        if (call_Counter<list.size){
//                            uploadFileNew(call_Counter++)
//
//                        }
//
//
//
//
//                    }
//                })
//    }

    /*
    private void getClassesAsync(final JobParameters params) {
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        String currentDateTime = DateTimeUtility.getFormattedDateTimeSlashFormatSansT(timeInMillis);

        UserServices mUserServices = new UserServicesImpl();
        mUserServices.getTodayClassesByInstructor(
                7,
                10824,
                "04/26/2021 13:00:39", //currentDateTime,
                new ResponseCallBackHandler<ArrayList<ScheduleModel>>() {
                    @Override
                    public void returnResponse(ResponseHandler<ArrayList<ScheduleModel>> responseHandler) {
                        AppLogger.Logger.info(TAG, "API response received.");
                        if (responseHandler.isExecuted()) {
                            ArrayList<ScheduleModel> models = responseHandler.getValue();
//                            updateList(models);
                        } else {
//                            onError(responseHandler.getMessage());
//                            setRecyclerViewVisibility();
                        }
                        makeNotification(ClockOutJobService.this);
                        jobFinished(params, false);
                        AppLogger.Logger.info(TAG, "Job finished.");
                        Toast.makeText(getApplicationContext(), "Job finished.!", Toast.LENGTH_SHORT).show();

                    }
                });
    }
*/
    private fun runClockOutApi(params: JobParameters) {
//        long timeInMillis = Calendar.getInstance().getTimeInMillis();
//        String currentDateTime = DateTimeUtility.getFormattedDateTimeSlashFormatSansT(timeInMillis);
//
//        UserSession mUserSession = MyApplication.getMyApplication().getUserSession();
//        LocationModel locationModel = mUserSession.getLocation();
//
//        InstructorClockOutDto dto = new InstructorClockOutDto();
//        dto.setProgramClassId(mClockedInClass.getProgramClassId());
//        dto.setLocation(locationModel.getLocation());
//        dto.setLongitude(locationModel.getLongitude());
//        dto.setLatitude(locationModel.getLatitude());
//        dto.setNote("");
//        dto.setCurrentDate(currentDateTime);
//
//        showLoading();
//      UserServices mUserServices = new UserServicesImpl();
//        mUserServices.clockOutInstructor(dto, new ResponseCallBackHandler() {
//            @Override
//            public void returnResponse(ResponseHandler responseHandler) {
//                hideLoading();
//                if (responseHandler.isExecuted()) {
//                    getClockedInSchedule();
//                } else {
//                    showInformationDialog(responseHandler.getMessage());
//                }
//            }
//        });
    }

    private fun makeNotification(context: Context) {
        createNotificationChannel()
        val builder = NotificationCompat.Builder(context, "notifyLe")
                .setSmallIcon(R.drawable.ic_folder_icon)
                .setContentTitle("Class Clock Out!")
                .setContentText("Your class has finished.")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        val notificationManagerCompat = NotificationManagerCompat.from(context)
        notificationManagerCompat.notify(200, builder.build())
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "CLockoutName"
            val description = "Channel for ClockOut"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("notifyLe", name, importance)
            channel.description = description
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        private val TAG ="my dddbbbb Service"
    }
}