package com.seekex.recordingapp.jobService;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.seekex.readstorage.jobService.RecordingsDBJobService;
import com.seekex.readstorage.jobService.RecordingsJobService;


public class RecordingsAlarmDbReceiver extends BroadcastReceiver {
    private static final String TAG = RecordingsAlarmDbReceiver.class.getSimpleName();
    private static final int JOB_ID = 123322;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive: Going to schedule job...");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scheduleJob(context);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void scheduleJob(Context context) {
        JobInfo jobInfo = getJob(context);
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

        int resultCode = scheduler.schedule(jobInfo);
        if (resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.e(TAG, " DB Job scheduled successfully.!");

        } else {
            Log.e(TAG, "Job scheduling failed.!");
//            Toast.makeText(context, "Job scheduling failed.!", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private JobInfo getJob(Context context) {
        ComponentName componentName = new ComponentName(context, RecordingsDBJobService.class);
        return new JobInfo.Builder(JOB_ID, componentName)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void cancelJob(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(JOB_ID);
        Log.e(TAG, "Job cancelled.!");
    }
}
