package com.seekex.recordingapp;

public class Contact {
    String _id;
    String _name;  
    String _phone_number;  
    String rec_file_path;

    public String getCandidate_id() {
        return candidate_id;
    }

    public void setCandidate_id(String candidate_id) {
        this.candidate_id = candidate_id;
    }

    String candidate_id;

    public String getCall_datetime() {
        return call_datetime;
    }

    public void setCall_datetime(String call_datetime) {
        this.call_datetime = call_datetime;
    }

    String call_datetime;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String status;



    String call_duration;

    public String getRec_number() {
        return rec_number;
    }

    public void setRec_number(String rec_number) {
        this.rec_number = rec_number;
    }

    String rec_number;
    String rec_url;
    String calldate;
    String calltime;

    public String getCalldate() {
        return calldate;
    }

    public void setCalldate(String calldate) {
        this.calldate = calldate;
    }

    public String getCalltime() {
        return calltime;
    }

    public void setCalltime(String calltime) {
        this.calltime = calltime;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    String call_type;

    public String getRec_url() {
        return rec_url;
    }

    public void setRec_url(String rec_url) {
        this.rec_url = rec_url;
    }

    public Contact(){   }
    public Contact(String  id, String name, String _phone_number, String rec_file_path){
        this._id = id;  
        this._name = name;  
        this._phone_number = _phone_number;  
        this.rec_file_path = rec_file_path;
    }
  
    public Contact(String name, String _phone_number, String call_duration, String rec_file_path, String status){
        this._name = name;  
        this._phone_number = _phone_number;  
        this.call_duration = call_duration;
        this.rec_file_path = rec_file_path;
        this.status = status;
    }

    public String getRec_file_path() {
        return this.rec_file_path;
    }

    public void setRec_file_path(String rec_file_path) {
        this.rec_file_path = rec_file_path;
    }

    public String getCall_duration() {
        return call_duration;
    }

    public void setCall_duration(String call_duration) {
        this.call_duration = call_duration;
    }

    public String getID(){
        return this._id;  
    }  
  
    public void setID(String id){
        this._id = id;  
    }  
  
    public String getName(){  
        return this._name;  
    }  
  
    public void setName(String name){  
        this._name = name;  
    }  
  
    public String getPhoneNumber(){  
        return this._phone_number;  
    }  
  
    public void setPhoneNumber(String phone_number){  
        this._phone_number = phone_number;  
    }  
}  